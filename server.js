'use strict';

// Dependencies
const express         = require('express');
const logger          = require('morgan');
const bodyParser      = require('body-parser');
const cookieParser    = require('cookie-parser');
const methodOverride  = require('method-override');
const helmet          = require('helmet');
const favicon         = require('serve-favicon');
const compression     = require('compression');
const session         = require('express-session');


const AuthRoute           = require('./app/routes/AuthRoute');
const UserRoute           = require('./app/routes/UserRoute');
const LocationRoute       = require('./app/routes/LocationRoute');
const EventRoute          = require('./app/routes/EventRoute');
const CheckRoute          = require('./app/routes/CheckRoute');
const NotificationRoute   = require('./app/routes/NotificationRoute');
const FileRoute           = require('./app/routes/FileRoute');
const EmailRoute          = require('./app/routes/EmailRoute');
const ActivityRoute       = require('./app/routes/ActivityRoute');
const PdfRoute            = require('./app/routes/PdfRoute');
const RequestRoute        = require('./app/routes/RequestRoute');

// Configuration
const app = express();

// disable default x-powered-by from express
app.disable('x-powered-by');

// setting up logging for dev environment
app.use(logger('dev'));

// http header stup for various attacks
app.use(helmet.xssFilter());
app.use(helmet.noCache());
app.use(helmet.noSniff());
app.use(helmet.frameguard());
app.use(helmet.hidePoweredBy());

// setting up body and cookie parsers.
app.use(bodyParser.json());
app.use(bodyParser.json({type: 'application/vnd.api+json'}));
app.use(bodyParser.urlencoded({'extended' : false}));
app.use(cookieParser());
app.use(methodOverride());


app.use(session({
  secret: process.env.SECRET_KEY,
  resave: true,
  saveUninitialized: true,
  maxAge: 73485734578
}));

app.use(express.static(__dirname + '/public'));

// setting up favicon icon for application.
app.use(favicon(__dirname + '/public/favicon.ico'));

app.get('/', function(req, res) {

  res.sendfile('./public/index.html');

});

// Setting up compression technique
app.use(compression());

// Errors handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', { message: err.message, error: err });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  console.log(err.message);
  res.render('error', { message: err.message, error: {} });
});

app.use('/api/auth', AuthRoute);
app.use('/api/users', UserRoute);
app.use('/api/locations', LocationRoute);
app.use('/api/events', EventRoute);
app.use('/api/checks', CheckRoute);
app.use('/api/notifications', NotificationRoute);
app.use('/api/files', FileRoute);
app.use('/api/email', EmailRoute);
app.use('/api/activity', ActivityRoute);
app.use('/api/pdf', PdfRoute);
app.use('/api/requests', RequestRoute);


// Connection
const port = process.env.PORT || 7000;
app.listen(port);

console.log('App listening on port ' + port);
