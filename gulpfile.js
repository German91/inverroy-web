'use strict';

// Dependencies
var gulp        = require('gulp'),
    sass        = require('gulp-sass'),
    nodemon     = require('gulp-nodemon'),
    cssnano     = require('gulp-cssnano'),
    jshint      = require('gulp-jshint'),
    uglify      = require('gulp-uglify'),
    imagemin    = require('gulp-imagemin'),
    rename      = require('gulp-rename'),
    concat      = require('gulp-concat'),
    notify      = require('gulp-notify'),
    del         = require('del'),
    stylish     = require('jshint-stylish'),
    changed     = require('gulp-changed'),
    rev         = require('gulp-rev'),
    browserSync = require('browser-sync');

// Sass
gulp.task('styles', function() {
  return gulp.src('public/src/css/style.sass')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('public/dist/css'))
    .pipe(rename({suffix: '.min'}))
    .pipe(cssnano())
    .pipe(gulp.dest('public/dist/css'))
    .pipe(notify({ message: 'CSS files complete' }));
});

// JSHint, concat, and minify
gulp.task('scripts', function() {
  return gulp.src('public/src/js/**/*.js')
    .pipe(jshint('.jshintrc'))
    .pipe(jshint.reporter('default'))
    .pipe(concat('main.js'))
    .pipe(gulp.dest('public/dist/js'))
    .pipe(rename({suffix: '.min'}))
    .pipe(uglify())
    .pipe(gulp.dest('public/dist/js'))
    .pipe(notify({ message: 'JS files complete' }));
});

// Compress Images
gulp.task('images', function() {
  return del(['dist/images']), gulp.src('public/src/images/**/*')
    .pipe(imagemin({ optimizationLevel: 3, progressive: true, interlaced: true }))
    .pipe(gulp.dest('public/dist/img'))
    .pipe(notify({ message: 'Images files complete' }));
});


//register nodemon task
gulp.task('nodemon', function() {
  nodemon({
    script: 'server.js',
    env: {
      'NODE_ENV': 'development'
    }
  }).on('restart');
});

// Clean Up
gulp.task('clean', function() {
  return del(['dist']);
});

// Default task
gulp.task('default', ['clean'], function() {
  gulp.start('nodemon', 'styles', 'scripts', 'images', 'browser-sync', 'watch');
});

// Watch
gulp.task('watch', ['browser-sync'], function() {

  // Watch .scss files
  gulp.watch('public/src/css/**/*.sass', ['styles']);

  // Watch .js files
  gulp.watch('public/src/js/**/*.js', ['scripts']);

  // Watch image files
  gulp.watch('public/src/images/**/*', ['images']);

});

gulp.task('browser-sync', ['nodemon'], function() {

  browserSync.init(null, {
    proxy: "http://localhost:7000",
        files: ["public/**/*.*"],
        port: 3000,
        notify: true
  });

  gulp.watch(['dist/**']).on('change', browserSync.reload);

});
