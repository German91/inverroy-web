'use strict';

angular.module('inverroy', [
  'ui.router',
  'angular-spinkit',
  'jcs-autoValidate',
  'ngFileUpload',
  'uiGmapgoogle-maps',
  'angularReverseGeocode',
  'vsGoogleAutocomplete',
  'simplePagination',
  'ngCsvImport',
  'htmlToPdfSave',
  'ngProgress'])
.run([
  'bootstrap3ElementModifier',
  function (bootstrap3ElementModifier) {
        bootstrap3ElementModifier.enableValidationStateIcons(true);
 }])
.config(function($stateProvider, $urlRouterProvider) {

  $stateProvider
    .state('login', {
      url: '/login',
      views: {

        'content' : {
          templateUrl : 'views/auth/login.html',
          controller  : 'AuthController'
        }

      }
  })
  .state('recover', {
      url: '/recover',
      views: {

          'content' : {
              templateUrl : 'views/auth/recover.html',
              controller  : 'AuthController'
          }

      }
  })
  .state('reset', {
    url: '/resetPassword:reset_password_token',
    views: {

      'content@' : {
        templateUrl: 'views/auth/reset.html',
        controller: 'AuthController'
      }

    }
  })
  .state('confirm', {
    url: '/confirmAccount',
    views: {

      'content@' : {
        templateUrl: 'views/auth/confirm.html',
        controller: 'AuthController'
      }

    }
  })
  .state('dashboard', {
      url: '/',
      views: {

          'header'  : {
              templateUrl : 'views/app/header.html'
          },

          'content' : {
              templateUrl : 'views/dashboard/index.html',
              controller  : 'DashboardController'
          }

      }
  })
  .state('dashboard.profile', {
      url: 'profile',
      views: {

          'content@' : {
              templateUrl : 'views/dashboard/profile.html',
              controller  : 'ProfileController'
          }

      }
  })
  .state('dashboard.users', {
      url: 'users',
      views: {

          'content@' : {
              templateUrl : 'views/dashboard/users.html',
              controller  : 'UsersController'
          }

      }
  })
  .state('dashboard.locations', {
      url: 'locations',
      views: {

          'content@' : {
              templateUrl : 'views/dashboard/locations.html',
              controller  : 'LocationsController'
          }

      }
  })
  .state('dashboard.incidents', {
      url: 'incidents',
      views: {

          'content@' : {
              templateUrl : 'views/dashboard/incidents.html',
              controller  : 'IncidentController'
          }

      }
  })
  .state('dashboard.documents', {
      url: 'documents',
      views: {

          'content@' : {
              templateUrl : 'views/dashboard/documents.html',
              controller  : 'DocumentsController'
          }

      }
  })
  .state('dashboard.requests', {
    url: 'requests',
    views: {

      'content@' : {
        templateUrl: 'views/dashboard/requests.html',
        controller: 'RequestController'
      }

    }
  });


  $urlRouterProvider.otherwise('/login');

});

'use strict';

angular.module('inverroy').controller('AuthController', ['$scope', 'AuthService', '$state', '$stateParams', function ($scope, AuthService, $state, $stateParams) {


    $('body').addClass('loginBackground');


    AuthService.checkUser();


    if ($scope.isLogged) {

        $state.go('dashboard');

    }

    $scope.login = function (user, loginForm) {

        AuthService.login(user);

    };

    $scope.recoverPassword = function (user, recoverForm) {

        var email = user.email;

        AuthService.recover(email).then(function (response) {

            $scope.showMessage('success', response.data.message);

            recoverForm.$setPristine();
            $scope.user = {};
            $scope.user.email = '';


        }, function (err) {

            $scope.showMessage('danger', response.data.message);

        });

    };

    $scope.setPassword = function (user) {

      user.reset_password_token = $stateParams.reset_password_token;

      AuthService.setPassword(user).then(function (response) {
        console.log(response);
      }, function (err) {
        console.log(err);
      });

    };

}]);

'use strict';

angular.module('inverroy').controller('DashboardController',
['$scope', 'AuthService', 'UserService', '$state', '$timeout', 'EventService', 'LocationService', 'CheckService', 'Pagination', 'NotificationService', 'ActivityService', 'PdfService', '$sce', 'FileService', 'Upload', 'EmailService', '$filter', 'ngProgressFactory',
function ($scope, AuthService, UserService, $state, $timeout, EventService, LocationService, CheckService, Pagination, NotificationService, ActivityService, PdfService, $sce, FileService, Upload, EmailService, $filter, ngProgressFactory) {

    $('body').removeClass('loginBackground');
    $('body').addClass('bg-content');

    AuthService.checkUser();


    if ($scope.isLogged) {

        $state.go('dashboard');

    } else {

        $state.go('login');

    }

    // Variables
    $scope.locations = [];
    $scope.users = [];
    $scope.numUsers = 15;
    $scope.pagination = Pagination.getNew($scope.numUsers);
    $scope.pagination2 = Pagination.getNew($scope.numUsers);
    $scope.locations = [];
    $scope.incident = {};
    $scope.toSend = {};
    $scope.toSend.ids = "";
    $scope.toSend.emails = "";
    $scope.incident.location = {};
    $scope.showButton = false;
    $scope.feeds = [];
    $scope.noShow = true;
    $scope.mess = {};
    $scope.sortType = 'user.first_name';
    $scope.sortReverse = false;
    $scope.note = {};
    $scope.emails = [];
    $scope.email = {};
    $scope.summaryText = {};
    $scope.progressbar = ngProgressFactory.createInstance();

    // Functions
    function getFeed() {

        $scope.loadingFeed = true;

        ActivityService.getFeed($scope.event.id).then(function (response) {
            $scope.loadingFeed = false;
            $scope.feeds = response.data.data;


            if ($scope.feeds) {
              $scope.pagination2.numPages = Math.ceil($scope.feeds.length/$scope.pagination2.perPage);
            }


        }, function (err) {

            $scope.loadingFeed = false;

        });

    }

    function checkEvent() {

        if ($scope.active) {

            EventService.getActive().then(function (response) {

                if (response.data.data.id || response.data.data.id !== undefined) {

                    var event_id = response.data.data.id;
                    getFeed();
                    $scope.checking = true;

                    CheckService.getCheks(event_id).then(function (response) {

                        $scope.users = [];

                        $scope.checking = false;
                        $scope.users = response.data.data;

                        if ($scope.users) {
                           $scope.pagination.numPages = Math.ceil($scope.users.length/$scope.pagination.perPage);
                        }


                    }, function (err) {

                        $scope.showMessage('danger', err.data.message.reason);
                        $scope.checking = false;

                    });

                }

            }, function (err) {

                $scope.showMessage('danger', err.data.message.reason);

            });

        }

    }

    function searchUsers() {

        $scope.users = [];

        $scope.checking = true;
        $scope.loadingFeed = true;

        setInterval(function () {

            var array = [];

            EventService.getActive().then(function (response) {

                if (response.data.data) {

                  if (response.data.data.id) {

                    var event_id = response.data.data.id;

                       ActivityService.getFeed($scope.event.id).then(function (response) {
                        $scope.feeds = response.data.data;

                        if ($scope.feeds) {
                          $scope.pagination2.numPages = Math.ceil($scope.feeds.length/$scope.pagination2.perPage);
                        }

                        $scope.loadingFeed = false;
                    });

                    CheckService.getCheks(event_id).then(function (response) {

                        $scope.users = [];

                        array = response.data.data;
                        $scope.checking = false;

                        if (array) {
                          if (array.length > $scope.users.length) {

                              $scope.users = array;

                              if ($scope.users) {
                                $scope.pagination.numPages = Math.ceil($scope.users.length/$scope.pagination.perPage);
                              }

                          }

                        }

                    }, function (err) {

                        $scope.checking = false;
                        $scope.showMessage('danger', err.data.message.reason);

                    });

                  }

                }

            }, function (err) {

                $scope.showMessage('danger', err.data.message.reason);

            });
        }, 5000);

    }

    function initialize() {

        $scope.geocoder = new google.maps.Geocoder();

        $scope.lat = 55.93334052458581;
        $scope.lon = -3.2139009644180305;
        $scope.map = {center: {latitude: 55.93334052458581, longitude: -3.2139009644180305 }, zoom: 14, control: {} };
        $scope.options = {scrollwheel: false};
        $scope.marker = {
          id: 0,
          coords: {
            latitude: 55.93334052458581,
            longitude: -3.2139009644180305
          },
          options: { draggable: true },
          events: {
            dragend: function (marker, eventName, args) {

              $scope.lat = marker.getPosition().lat();
              $scope.lon = marker.getPosition().lng();

              var latlng = new google.maps.LatLng($scope.lat, $scope.lon);

              $scope.geocoder.geocode({ 'latLng': latlng }, function (results, status) {

                $scope.incident.location.address = results[0].formatted_address;
                $scope.incident.location.latitude = $scope.lat;
                $scope.incident.location.longitude = $scope.lon;

                for (var address in results[0].address_components) {

                    if (results[0].address_components[address].types[0] === "country") {

                        $scope.incident.location.country =  results[0].address_components[address].long_name;

                    }

                }

              });

              $scope.marker.options = {
                draggable: true,
                labelContent: $scope.incident.location.address,
                labelAnchor: "100 0",
                labelClass: "marker-labels"
              };
            }
          }
        };

    }


    function getLocations() {

        $scope.loadinglocations = true;

        LocationService.getAll().then(function (response) {

            $scope.loadinglocations = false;
            $scope.locations = response.data.data;

        }, function (err) {

            $scope.loadinglocations = false;
            $scope.showMessage('danger', err.data.message.reason);

        });

    }


    $scope.changeNumUsers = function (num) {

      $scope.numUsers = num;
      $scope.pagination = Pagination.getNew($scope.numUsers);

    };


    $scope.getAddress = function (address) {

        $scope.geocoder.geocode({ 'address': address }, function (results, status) {

            if (status === google.maps.GeocoderStatus.OK) {

                $scope.newcoords = results[0].geometry.location;

                $scope.marker.coords.latitude = $scope.newcoords.lat();
                $scope.marker.coords.longitude = $scope.newcoords.lng();

                $scope.map.center.latitude = $scope.newcoords.lat();
                $scope.map.center.longitude = $scope.newcoords.lng();

                $scope.incident.location.address = results[0].formatted_address;
                $scope.incident.location.latitude = $scope.newcoords.lat();
                $scope.incident.location.longitude = $scope.newcoords.lng();

                $scope.$apply();

                for (var address in results[0].address_components) {

                    if (results[0].address_components[address].types[0] === "country") {

                        $scope.incident.location.country =  results[0].address_components[address].long_name;

                    }

                }
            }

        });

    };

    $scope.changeIcon = function(id) {

        var id = '#span_' + id;

        var icon = $(id);

        if (icon.hasClass('glyphicon-menu-right')) {

            icon.removeClass('glyphicon-menu-right');
            icon.addClass('glyphicon-menu-down');

        } else if (icon.hasClass('glyphicon-menu-down')) {

            icon.removeClass('glyphicon-menu-down');
            icon.addClass('glyphicon-menu-right');

        }

    };

    $scope.createIncident = function (incident) {

        var newIncident = {};
        $scope.creatingincident = true;

        if (!incident.selected || incident.selected === null) {

            newIncident.hasTemporary = true;

            newIncident.name                = incident.name;
            newIncident.description         = incident.description;
            newIncident.phone               = incident.phone;
            newIncident.temporary_name      = incident.location.address;
            newIncident.temporary_address   = incident.location.address;
            newIncident.temporary_country   = incident.location.country;
            newIncident.temporary_latitude  = incident.location.latitude;
            newIncident.temporary_longitude = incident.location.longitude;

            EventService.createEvent(newIncident).then(function (response) {

                $scope.creatingincident = false;
                $scope.showMessage('success', response.data.message);

                $scope.checkStatus();
                checkEvent();
                getLocations();

            }, function (err) {

                $scope.creatingincident = false;
                $scope.showMessage('danger', err.data.message.reason);

            });

        } else {

            incident.place_id = incident.selected;

            EventService.createEvent(incident).then(function (response) {

                $scope.creatingincident = false;
                $scope.showMessage('success', response.data.message);
                $scope.checkStatus();

            }, function (err) {

                $scope.creatingincident = false;
                $scope.showMessage('danger', err.data.message.reason);

            });

        }

    };

    $scope.getAddress2 = function (address) {

        $scope.geocoder.geocode({ 'address': address }, function (results, status) {

            if (status === google.maps.GeocoderStatus.OK) {

                $scope.newcoords = results[0].geometry.location;

                $scope.marker2.coords.latitude = $scope.newcoords.lat();
                $scope.marker2.coords.longitude = $scope.newcoords.lng();

                $scope.map2.center.latitude = $scope.newcoords.lat();
                $scope.map2.center.longitude = $scope.newcoords.lng();

                if (!$scope.event.custom_fields.hasTemporary && $scope.currentEvent.place.id) {

                    $scope.event.place.address = results[0].formatted_address;
                    $scope.event.place.latitude = $scope.newcoords.lat();
                    $scope.event.place.longitude = $scope.newcoords.lng();

                    for (var address in results[0].address_components) {

                        if (results[0].address_components[address].types[0] === "country") {

                            $scope.event.place.country =  results[0].address_components[address].long_name;

                        }

                    }

                } else {

                    $scope.newplace.address =  results[0].formatted_address;
                    $scope.newplace.latitude = $scope.newcoords.lat();
                    $scope.newplace.longitude = $scope.newcoords.lng();

                    for (var address in results[0].address_components) {

                        if (results[0].address_components[address].types[0] === "country") {

                            $scope.newplace.country =  results[0].address_components[address].long_name;

                        }

                    }

                }

                $scope.$apply();
            }

        });

    };

    $scope.stopIncident = function () {

        EventService.stopIncident($scope.event.id).then(function (response) {

            $scope.showMessage('success', response.data.message);
            $scope.checkStatus();
            $('#stopModal').modal('hide');

        }, function (err) {

            $scope.showMessage('danger', err.data.message.reason);
            $('#stopModal').modal('hide');

        });

    };

    $scope.updateIncident = function (incident) {

        $scope.updatingevent = true;
        $scope.toUpdate = {};

        $scope.toUpdate.event_id = incident.id;
        $scope.toUpdate.name = incident.name;
        $scope.toUpdate.phone = incident.custom_fields.phone;
        $scope.toUpdate.details = incident.details;

        if (incident.place && incident.place.id) {

            $scope.toUpdate.hasTemporary = false;
            $scope.toUpdate.place_id = incident.place.id;

        } else {
            $scope.toUpdate.hasTemporary = true;

            $scope.toUpdate.temporary_name      = $scope.newplace.address;
            $scope.toUpdate.temporary_address   = $scope.newplace.address;
            $scope.toUpdate.temporary_country   = $scope.newplace.country;
            $scope.toUpdate.temporary_latitude  = $scope.newplace.latitude;
            $scope.toUpdate.temporary_longitude = $scope.newplace.longitude;

        }

        EventService.updateIncident($scope.toUpdate).then(function (response) {

            $scope.updatingevent = false;
            $scope.showMessage('success', response.data.message);

            $scope.checkStatus();
            $('#editincidentModal').modal('hide');

        }, function (err) {

            $scope.updatingevent = false;
            $scope.showMessage('danger', err.data.message.reason);

        });

    };

    $scope.editIncident = function () {

        $scope.showMap = false;
        $scope.currentEvent = {};
        $scope.newplace = {};
        $scope.loadingEvent = true;
        $scope.loadingActive = true;

        if ($scope.event.custom_fields.hasTemporary) {

          $scope.newplace.address   = $scope.event.custom_fields.temporary_address;
          $scope.newplace.latitude  = $scope.event.custom_fields.temporary_latitude;
          $scope.newplace.longitude = $scope.event.custom_fields.temporary_longitude;
          $scope.newplace.country   = $scope.event.custom_fields.temporary_country;
          $scope.newplace.name      = $scope.event.custom_fields.temporary_name;

        }

        EventService.getActive().then(function (response) {
            $scope.loadingEvent = false;
            $scope.currentEvent = response.data.data;
            $scope.loadingActive = false;

            if ($scope.currentEvent.custom_fields.hasTemporary) {

                if ($scope.currentEvent.place) {

                    $scope.currentEvent.place = "";

                }

            }

            $('#editincidentModal').modal('show');

                if ($scope.currentEvent.custom_fields.hasTemporary) {

                    $timeout(function () {
                        $scope.map2 = {};
                        $scope.marker2 = {};
                        $scope.showMap = true;
                        $scope.map2 = {center: {latitude: $scope.currentEvent.custom_fields.temporary_latitude, longitude: $scope.currentEvent.custom_fields.temporary_longitude }, zoom: 14, control: {} };
                        $scope.marker2 = {
                          id: 0,
                          coords: {
                            latitude: $scope.currentEvent.custom_fields.temporary_latitude,
                            longitude: $scope.currentEvent.custom_fields.temporary_longitude
                          },
                          options: { draggable: true },
                          events: {
                            dragend: function (marker2, eventName, args) {

                              $scope.lat = marker2.getPosition().lat();
                              $scope.lon = marker2.getPosition().lng();

                              var latlng = new google.maps.LatLng($scope.lat, $scope.lon);

                              $scope.geocoder.geocode({ 'latLng': latlng }, function (results, status) {

                                $scope.newplace.address = results[0].formatted_address;

                                for (var address in results[0].address_components) {

                                    if (results[0].address_components[address].types[0] === "country") {

                                        $scope.newplace.country =  results[0].address_components[address].long_name;

                                    }

                                }


                              });

                              $scope.marker2.options = {
                                draggable: true,
                                labelContent: $scope.newplace.address,
                                labelAnchor: "100 0",
                                labelClass: "marker-labels"
                              };
                            }
                          }
                        };

                    }, 1000);


                } else {

                    $timeout(function () {
                        $scope.map2 = {};
                        $scope.marker2 = {};
                        $scope.showMap = true;
                        $scope.map2 = {center: {latitude: $scope.currentEvent.place.latitude, longitude: $scope.currentEvent.place.longitude }, zoom: 14, control: {} };
                        $scope.marker2 = {
                          id: 0,
                          coords: {
                            latitude: $scope.currentEvent.place.latitude,
                            longitude:  $scope.currentEvent.place.longitude
                          },
                          options: { draggable: true },
                          events: {
                            dragend: function (marker2, eventName, args) {

                              $scope.lat = marker2.getPosition().lat();
                              $scope.lon = marker2.getPosition().lng();

                              var latlng = new google.maps.LatLng($scope.lat, $scope.lon);

                              $scope.geocoder.geocode({ 'latLng': latlng }, function (results, status) {

                                $scope.newplace.address = results[0].formatted_address;

                                for (var address in results[0].address_components) {

                                    if (results[0].address_components[address].types[0] === "country") {

                                        $scope.newplace.country =  results[0].address_components[address].long_name;

                                    }

                                }


                              });

                              $scope.marker2.options = {
                                draggable: true,
                                labelContent: $scope.newplace.address,
                                labelAnchor: "100 0",
                                labelClass: "marker-labels"
                              };
                            }
                          }
                        };

                    }, 1000);


                }

        }, function (err) {

            $scope.loadingEvent = false;
            $scope.showMessage('danger', err.data.message.reason);

        });

    };


    $scope.stopModal = function () {

        $('#stopModal').modal('show');

    };

    $scope.closeStop = function () {

        $('#stopModal').modal('hide');

    };

    function getRoleEmail(role) {

        for (var user in $scope.users) {

          if ($scope.users[user].user.custom_fields.crisis_role === role) {

              if ($scope.users[user].user.email !== undefined) {
                $scope.toSend.emails += $scope.users[user].user.email + ',';
              }

          }

        }

    }

    $scope.summary = function (summaryData, form) {

      if ($scope.email.newEmail !== '') {
        $scope.emails.push($scope.email.newEmail);
      }

      $scope.sendingSummary = true;
      $scope.progressbar.start();

      $.notify({

        message: 'We are creating the summary, please do not close that page until the progress be finished.'

      }, {

        type: 'warning',
        animate: {
          enter: 'animated fadeInRight',
          exit: 'animated fadeOutRight'
        },
        offset: {
          x: 50,
          y: 100
        },
        delay: 16000

      });

      form.$setPristine();
      $scope.summaryText        = {};
      $scope.summaryText.text   = '';
      $scope.summaryText.member = false;
      $scope.summaryText.cmt    = false;
      $scope.summaryText.chair  = false;
      $scope.summaryText.other  = false;

      $('#sendSummaryModal').modal('hide');

        var users = {

            usersEmail:     [],
            usersDept:      [],
            usersMobile:    [],
            usersWork:      [],
            usersHome:      [],
            usersMessage:   [],

        };

        var feeds = {

          feedsType: [],
          feedsTime: [],
          feedsWho: [],
          feedsDescription: []

        };

        var incident = {};

        incident.details = $scope.event.details;

        if (!$scope.event.custom_fields.hasTemporary) {

          incident.name     = $scope.event.name;
          incident.address  = $scope.event.place.address;
          incident.country  = $scope.event.place.country;

        } else {

          incident.name     = ' ';
          incident.address  = $scope.event.custom_fields.temporary_address;
          incident.country  = $scope.event.custom_fields.temporary_country;

        }

        for (var user in $scope.users) {

            users.usersEmail.push($scope.users[user].user.email);
            users.usersDept.push($scope.users[user].user.custom_fields.department);
            users.usersMobile.push($scope.users[user].user.custom_fields.mobile_phone);
            users.usersWork.push($scope.users[user].user.custom_fields.work_phone);
            users.usersHome.push($scope.users[user].user.custom_fields.home_phone);
            users.usersMessage.push($scope.users[user].message);

        }

        function generateFeed () {

          var feeds = [];

          if ($scope.feeds.length > 0) {

            for (var feed in $scope.feeds) {

              $scope.feeds[feed].time = $filter('date')($scope.feeds[feed].time, "dd/MM/yyyy h:mm a");

              feeds.push(
                { text: $scope.feeds[feed].time },
                { text: 'To: ' + $scope.feeds[feed].who },
                ' ',
                { text: $scope.feeds[feed].text },
                ' '
              );

            }

          }

          return feeds;

        }

        for (var user in users.usersEmail) {

          if (users.usersEmail[user] === undefined) {
            users.usersEmail[user] = 'none';
          }

        }

        for (var user in users.usersDept) {

          if (users.usersDept[user] === undefined) {
            users.usersDept[user] = 'none';
          }

        }

        for (var user in users.usersMobile) {

          if (users.usersMobile[user] === undefined) {
            users.usersMobile[user] = 'none';
          }

        }

        for (var user in users.usersWork) {

          if (users.usersWork[user] === undefined) {
            users.usersWork[user] = 'none';
          }

        }

        for (var user in users.usersHome) {

          if (users.usersHome[user] === undefined) {
            users.usersHome[user] = 'none';
          }

        }

        for (var user in users.usersMessage) {

          if (users.usersMessage[user] === undefined) {
            users.usersMessage[user] = 'none';
          }

        }


        var table = {
            pageOrientation: 'landscape',
            content: [
              { text: incident.name },
              { text: incident.address },
              { text: incident.country },
              ' ',
              incident.details,
              ' ',
              { text: 'Who is Available', style: 'header' },
              ' ',
                {
                    table: {
                        headerRows: 1,
                        widths: [180, 50, 100, 100, 100, 100],
                        body: [
                            ['Email', 'Dept', 'Mobile', 'Work', 'Home', 'Availability'],
                            [users.usersEmail, users.usersDept, users.usersMobile, users.usersWork, users.usersHome, users.usersMessage]
                        ]
                    },
                    layout: 'lightHorizontalLines',
                    pageBreak: 'after'
                },
                ' ',
                { text: 'Activity Feed', style: 'header' },
                ' ',
                generateFeed()
            ],

        };

        pdfMake.createPdf(table).getBase64(function(dataURL) {

          var file = {
            data: dataURL,
            eventId: $scope.event.id,
            email: $scope.profile.email
          };


          PdfService.createPdf(file).then(function (response) {

            var file_id = response.data.data.id;

            FileService.searchFile(file_id).then(function (response) {

                var sentTo = "";

                if (summaryData.cmt) {

                    var role = "CMT";
                    sentTo += "CMT" + ', ';
                    getRoleEmail(role);

                }

                if (summaryData.member) {

                    var role = "BRTMember";
                    sentTo += "BRTMember" + ', ';
                    getRoleEmail(role);

                }

                if (summaryData.chair) {

                    var role = "BRTChair";
                    sentTo += "BRTChair" + ', ';
                    getRoleEmail(role);

                }

                if (summaryData.other) {

                    var role = "other";
                    sentTo += "other" + ', ';
                    getRoleEmail(role);

                }

                if ($scope.emails.length > 0) {

                  for (var email in $scope.emails) {

                    if ($scope.emails[email] !== undefined) {
                      $scope.toSend.emails += $scope.emails[email] + ', ';
                    }

                  }

                }

                var data = {
                  file: response.data.data.id,
                  message: summaryData.message,
                  who: $scope.toSend.emails
                };

                $scope.emails = [];


                EmailService.sendSummary(data).then(function (response) {

                  $scope.showMessage('success', 'Created summary successful');
                  $scope.sendingSummary = false;
                  $scope.progressbar.complete();

                }, function (err) {

                  $scope.sendingSummary = false;
                  $scope.progressbar.complete();
                  $scope.showMessage('danger', err.data.message.reason);

                });


            });

          }, function (err) {

            $scope.showMessage('danger', err.data.message.reason);

          });

        });


    };

    function getRole(role) {

        for (var user in $scope.users) {

          if ($scope.users[user].user.custom_fields.crisis_role === role) {

              $scope.toSend.ids += $scope.users[user].user.id + ',';

          }

        }

    }


    $scope.sendMessage = function (mess, form) {

      form.$setPristine();
      $scope.mess = {};
      $scope.toSend.text = mess.text;
      $scope.toSend.ids = '';

        var sentTo = "";

        if (mess.cmt) {

            var role = "CMT";
            sentTo += "CMT" + ', ';
            getRole(role);

        }

        if (mess.member) {

            var role = "BRTMember";
            sentTo += "BRTMember" + ', ';
            getRole(role);

        }

        if (mess.chair) {

            var role = "BRTChair";
            sentTo += "BRTChair" + ', ';
            getRole(role);

        }

        if (mess.other) {

            var role = "other";
            sentTo += "other" + ', ';
            getRole(role);

        }


        if ($scope.toSend.ids === "") {

          $scope.showMessage('danger', 'No Active Users - There are currently no users available. This message will be added to the activity feed and users will receive this information if they become available');

           var message = {};

           message.type = "notification";
           message.time = new Date();
           message.who = sentTo;
           message.message = $scope.toSend.text;
           message.id = $scope.profile.id;
           message.event = $scope.event.id;

          ActivityService.createFeed(message).then(function (response){

            getFeed();

          });

        } else {


          NotificationService.pushNotification($scope.toSend).then(function (response) {

             $scope.sending = false;
             $scope.showMessage('success', response.data.message);

             var message = {};

             message.type = "notification";
             message.time = new Date();
             message.who = sentTo;
             message.message = $scope.toSend.text;
             message.id = $scope.profile.id;
             message.event = $scope.event.id;

             ActivityService.createFeed(message).then(function (response){

                 getFeed();

             });

          }, function (err) {

             $scope.sending = false;
             $scope.showMessage('danger', err.data.message.reason);

          });

        }

    };

    $scope.openNote = function () {

        $('#noteModal').modal('show');

    };

    $scope.closeNote = function () {

        $scope.note = {};
        $scope.note.message = '';
        $('#noteModal').modal('hide');

    };

    $scope.addNote = function(note) {

        $scope.adding = true;

        var message = {};

        message.type = "note";
        message.time = new Date();
        message.who = "Note";
        message.message = note.message;
        message.id = $scope.profile.id;
        message.event = $scope.event.id;

        ActivityService.createFeed(message).then(function (response){

            $scope.adding = false;
            $scope.showMessage('success', response.data.message);
            $scope.closeNote();

            getFeed();

        }, function (err) {

            $scope.adding = false;
            $scope.showMessage('danger', err.data.message.reason);
            $scope.closeNote();

        });

    };

    $scope.openSummaryModal = function () {

      $('#sendSummaryModal').modal('show');

    };

    $scope.addEmail = function (email, form) {

      $scope.emails.push(email.newEmail);

      form.$setPristine();
      $scope.email.newEmail = '';
      $scope.email = {};

    };

    $scope.editEmail = function (email) {

      var index = $scope.emails.indexOf(email);
      $scope.emails[index] = email;

    };

    $scope.removeEmail = function (email) {

      var index = $scope.emails.indexOf(email);
      $scope.emails.splice(index, 1);

    };

    // Use Functions
    getLocations();
    $scope.checkStatus();

    $scope.loading = true;

    $(document).ready(function () {

        initialize();
        $scope.loading = false;

    });

    searchUsers();
    checkEvent();
    getFeed();

}]);

'use strict';

angular.module('inverroy').controller('DocumentsController', ['$scope', '$state', 'AuthService', 'Upload', 'FileService', 'Pagination', function ($scope, $state, AuthService, Upload, FileService, Pagination) {

    AuthService.checkUser();

    if ($scope.profile.role == "Admin" || $scope.profile.role == "Super Admin") {

        if ($scope.isLogged) {

            $state.go('dashboard.documents');

        } else {

            $state.go('dashboard');

        }

    } else {

        $state.go('login');

    }

    // Variables
    $scope.allfiles = [];
    $scope.currentDocument = {};
    $scope.pagination = Pagination.getNew(15);

    function getFiles() {

      $scope.allfiles = [];

        $scope.loadingFiles = true;

        FileService.getFiles().then(function (response) {

            $scope.loadingFiles = false;

            for (var file in response.data.data) {

              if ($scope.profile.custom_fields.crisis_role === 'BRTChair' && response.data.data[file].custom_fields.brtchair) {

                $scope.allfiles.push(response.data.data[file])

              }

              if ($scope.profile.custom_fields.crisis_role === 'BRTMember' && response.data.data[file].custom_fields.brtmember) {

                $scope.allfiles.push(response.data.data[file])

              }

              if ($scope.profile.custom_fields.crisis_role === 'other' && response.data.data[file].custom_fields.other) {

                $scope.allfiles.push(response.data.data[file])

              }

              if ($scope.profile.custom_fields.crisis_role === 'CMT' && response.data.data[file].custom_fields.cmt) {

                $scope.allfiles.push(response.data.data[file])

              }

            }

            if ($scope.allfiles) {
              $scope.pagination.numPages = Math.ceil($scope.allfiles.length/$scope.pagination.perPage);
            }


        }, function (err) {

            $scope.loadingFiles = false;
            $scope.showMessage('danger', err.data.message.reason);

        });

    }

    $scope.uploadFile = function (file) {

        $scope.loadingImage = true;

        $scope.upload = Upload.upload({

          url     : '/api/files/',
          method  : 'POST',
          data    : { file: file }

        }).then(function(response) {

           $scope.loadingImage = false;
           $scope.showMessage('success', response.data.message);

           getFiles();

        }, function (err) {

            $scope.loadingImage = false;
            $scope.showMessage('danger', err.data.message.reason);

        });

    };

    $scope.deleteFile = function () {

        $scope.deletingFile = true;

        FileService.deleteFile($scope.currentDocument.id).then(function (response) {

            $scope.deletingFile = false;
            $scope.showMessage('success', response.data.message);

            getFiles();
            $('#removefileModal').modal('hide');

        }, function (err) {

            $scope.deletingFile = false;
            $scope.showMessage('danger', err.data.message.reason);
            $('#removefileModal').modal('hide');

        });

    };

    $scope.documentModal = function (file_id) {

        FileService.searchFile(file_id).then(function (response) {

            $scope.currentDocument = response.data.data;

            $('#documentModal').modal('show');

        });

    };

    $scope.updateFile = function (currentDocument) {

        $scope.updatingFile = true;

        var file_id = currentDocument.id;
        var file = {};

        file.name = currentDocument.name;
        file.brtmember = currentDocument.custom_fields.brtmember;
        file.brtchair = currentDocument.custom_fields.brtchair;
        file.cmt = currentDocument.custom_fields.cmt;
        file.other = currentDocument.custom_fields.other;

        FileService.updateFile(file_id, file).then(function (response) {

            $scope.updatingFile = false;
            $scope.showMessage('success', response.data.message);

            $('#documentModal').modal('hide');

            getFiles();

        }, function (err) {

            $scope.updatingFile = false;
            $scope.showMessage('danger', err.data.message.reason);

        });

    };

    $scope.openModal = function(file_id) {

      FileService.searchFile(file_id).then(function (response) {

            $scope.currentDocument = response.data.data;
            $('#removefileModal').modal('show');

        });

    };

    $scope.closeModal = function () {

      $scope.currentDocument = {};
      $('#removefileModal').modal('hide');

    };


    getFiles();


}]);

'use strict';

angular.module('inverroy').controller('IncidentController', [
  '$scope',
  '$state',
  'AuthService',
  'EventService',
  'Pagination',
  'EmailService',
  'RequestService',
  'FileService',

  function ($scope, $state, AuthService, EventService, Pagination, EmailService, RequestService, FileService) {

    AuthService.checkUser();

    if ($scope.isLogged) {

        $state.go('dashboard.incidents');

    } else {

        $state.go('login');

    }

    // Variables
    $scope.incidents = [];
    $scope.incident = {};
    $scope.file = {};
    $scope.loadingincidents = true;
    $scope.pagination = Pagination.getNew(15);

    function getAll() {

      EventService.getAll().then(function (response) {

          $scope.loadingincidents = false;
          $scope.incidents = response.data.data;

          if ($scope.incidents) {
            $scope.pagination.numPages = Math.ceil($scope.incidents.length/$scope.pagination.perPage);
          }


      }, function (err) {

          $scope.loadingincidents = false;
          $scope.showMessage('danger', err.data.message.reason);

      });

    }

    // Functions
    $scope.removeIncident = function (id) {

      swal({
        title: 'Are you sure?',
        text: "The incident will be archived. You will be able to see it in the View Archive section",
        type: 'warning',
        showCancelButton: true,
        buttonsStyling: false,
        confirmButtonClass: 'btn button-blue margin-right',
        cancelButtonClass: 'btn button-red',
        confirmButtonText: 'Yes, archive it!'
      }).then(function() {
        var eventId = id;

        EventService.remove(eventId).then(function (response) {

          $scope.showMessage('success', response.data.message);
          getAll();


        }, function (err) {

          $scope.showMessage('danger', err.data.message.reason);

        });
      })

    };


    $scope.openModal = function (id) {

      $scope.file = {};

      FileService.download(id).then(function (response) {

          // window.open(response.data.data.url, '_blank');
          $scope.file = response.data.data;

      }, function (err) {

        $scope.showMessage('danger', err.data.message.reason);

      });

      $('#openFileModal').modal('show');

    };

    $scope.closeModal = function () {

      $scope.file = {};
      $('#openFileModal').modal('hide');

    };

    $scope.openFile = function () {

      window.open($scope.file.url, '_blank');
      $('#openFileModal').modal('hide');

    };

    getAll();

}]);

'use strict';

angular.module('inverroy').controller('LocationsController', ['$scope', '$state', 'AuthService', 'LocationService', '$timeout', 'Pagination', function ($scope, $state, AuthService, LocationService, $timeout, Pagination) {

    AuthService.checkUser();

    if ($scope.profile.role == "Admin" || $scope.profile.role == "Super Admin") {

        if ($scope.isLogged) {

            $state.go('dashboard.locations');

        } else {

            $state.go('dashboard');

        }

    } else {

        $state.go('login');

    }

    // Variables
    var geocoder = new google.maps.Geocoder();
    $scope.locations = [];
    $scope.location = {};
    $scope.lat = 55.93334052458581;
    $scope.lon = -3.2139009644180305;
    $scope.map = {center: {latitude: 55.93334052458581, longitude: -3.2139009644180305 }, zoom: 14, control: {} };
    $scope.options = {scrollwheel: false};
    $scope.showMap = false;
    $scope.pagination = Pagination.getNew(15);

    $scope.marker = {
      id: 0,
      coords: {
        latitude: 55.93334052458581,
        longitude: -3.2139009644180305
      },
      options: { draggable: true },
      events: {
        dragend: function (marker, eventName, args) {

          $scope.lat = marker.getPosition().lat();
          $scope.lon = marker.getPosition().lng();

          var latlng = new google.maps.LatLng($scope.lat, $scope.lon);

          geocoder.geocode({ 'latLng': latlng }, function (results, status) {

            $scope.location.address = results[0].formatted_address;

            for (var address in results[0].address_components) {

                if (results[0].address_components[address].types[0] === "country") {

                    $scope.location.country =  results[0].address_components[address].long_name;

                }

            }


          });

          $scope.marker.options = {
            draggable: true,
            labelContent: $scope.location.address,
            labelAnchor: "100 0",
            labelClass: "marker-labels"
          };
        }
      }
    };

    $scope.getAddress = function (address) {

        geocoder.geocode({ 'address': address }, function (results, status) {

            if (status === google.maps.GeocoderStatus.OK) {

                $scope.newcoords = results[0].geometry.location;

                $scope.marker.coords.latitude = $scope.newcoords.lat();
                $scope.marker.coords.longitude = $scope.newcoords.lng();

                $scope.map.center.latitude = $scope.newcoords.lat();
                $scope.map.center.longitude = $scope.newcoords.lng();

                $scope.location.address = results[0].formatted_address;
                $scope.location.latitude = $scope.newcoords.lat();
                $scope.location.longitude = $scope.newcoords.lng();

                $scope.$apply();

                for (var address in results[0].address_components) {

                    if (results[0].address_components[address].types[0] === "country") {

                        $scope.location.country =  results[0].address_components[address].long_name;

                    }

                }
            }

        });

    };

    $scope.getAddress2 = function (address) {

        geocoder.geocode({ 'address': address }, function (results, status) {

            if (status === google.maps.GeocoderStatus.OK) {

                $scope.newcoords = results[0].geometry.location;

                $scope.marker2.coords.latitude = $scope.newcoords.lat();
                $scope.marker2.coords.longitude = $scope.newcoords.lng();

                $scope.map2.center.latitude = $scope.newcoords.lat();
                $scope.map2.center.longitude = $scope.newcoords.lng();

                $scope.currentlocation.address = results[0].formatted_address;
                $scope.currentlocation.latitude = $scope.newcoords.lat();
                $scope.currentlocation.longitude = $scope.newcoords.lng();

                $scope.$apply();

                for (var address in results[0].address_components) {

                    if (results[0].address_components[address].types[0] === "country") {

                        $scope.currentlocation.country =  results[0].address_components[address].long_name;

                    }

                }
            }

        });

    };

    function getLocations() {

        $scope.loadinglocations = true;

        LocationService.getAll().then(function (response) {

            $scope.loadinglocations = false;
            $scope.locations = response.data.data;

            if ($scope.locations) {
              $scope.pagination.numPages = Math.ceil($scope.locations.length/$scope.pagination.perPage);
            }


        }, function (err) {

            $scope.loadinglocations = false;
            $scope.showMessage('danger', err.data.message.reason);

        });

    }

    $scope.createLocation = function (location) {

        $scope.creatinglocation = true;

        LocationService.create($scope.location).then(function (response) {

            $scope.creatinglocation = false;
            $scope.showMessage('success', response.data.message);

            getLocations();

        }, function (err) {

            $scope.creatinglocation = false;
            $scope.showMessage('danger', err.data.message.reason);

        });

    };

    $scope.updateLocation = function (location) {

        $scope.updatingLocation = true;

        LocationService.updateLocation(location.id, location).then(function (response) {

            $scope.updatingLocation = false;
            $scope.showMessage('success', response.data.message);

            getLocations();

            $('#editlocationModal').modal('hide');

        }, function (err) {

            $scope.updatingLocation = false;
            $scope.showMessage('danger', err.data.message.reason);

        });


    };


    $scope.deleteLocation = function (placeId) {

        $scope.removingLocation = true;

        LocationService.deleteLocation(placeId).then(function (response) {

            $scope.removingLocation = false;
            getLocations();

        }, function (err) {

            $scope.removingLocation = false;
            $scope.showMessage('danger', err.data.message.reason);

        });

    };

    $scope.editLocation = function (location) {

        $scope.showMap = false;
        $scope.currentlocation = {};
        $scope.loadingPlace = true;

        LocationService.getPlace(location.id).then(function(response) {

            $scope.loadingPlace = false;
            $scope.currentlocation = response.data.data;

        }, function (err) {

            $scope.loadingPlace = false;
            $scope.showMessage('danger', err.data.message.reason);

        });



        $('#editlocationModal').modal('show');



        $timeout(function () {
            $scope.map2 = {};
            $scope.marker2 = {};
            $scope.showMap = true;
            $scope.map2 = {center: {latitude: $scope.currentlocation.latitude, longitude: $scope.currentlocation.longitude }, zoom: 14, control: {} };
            $scope.marker2 = {
              id: 0,
              coords: {
                latitude: $scope.currentlocation.latitude,
                longitude:  $scope.currentlocation.longitude
              },
              options: { draggable: true },
              events: {
                dragend: function (marker2, eventName, args) {

                  $scope.lat = marker2.getPosition().lat();
                  $scope.lon = marker2.getPosition().lng();

                  var latlng = new google.maps.LatLng($scope.lat, $scope.lon);

                  geocoder.geocode({ 'latLng': latlng }, function (results, status) {

                    $scope.currentlocation.address = results[0].formatted_address;

                    for (var address in results[0].address_components) {

                        if (results[0].address_components[address].types[0] === "country") {

                            $scope.currentlocation.country =  results[0].address_components[address].long_name;

                        }

                    }


                  });

                  $scope.marker2.options = {
                    draggable: true,
                    labelContent: $scope.currentlocation.address,
                    labelAnchor: "100 0",
                    labelClass: "marker-labels"
                  };
                }
              }
            };

        }, 1000);

    };

    getLocations();


}]);

'use strict';

angular.module('inverroy').controller('MainController', ['$scope', 'AuthService', '$state', '$rootScope', 'EventService', '$timeout', function ($scope, AuthService, $state, $rootScope, EventService, $timeout) {

    AuthService.checkUser();

    $scope.$watch(AuthService.logged, function (logged) {

        $scope.loading = true;

        $scope.isLogged = logged;
        $scope.profile  = AuthService.currentUser();


        if (!$scope.isLogged) {

            $state.go('login');

        } else {

            $scope.loading = false;
            $state.go('dashboard');

        }

    });

    $scope.logout = function () {

        AuthService.logout();

    };

    $scope.$watch('message', function (newValue, oldValue) {

        if (newValue) {

            $scope.message = {};

        }

    });

    $scope.showMessage = function (type, message) {

      $.notify({

        message: message

      }, {

        type: type,
        animate: {
          enter: 'animated fadeInRight',
          exit: 'animated fadeOutRight'
        },
        offset: {
          x: 50,
          y: 100
        },
        delay: 2000

      });

    };

    // Variables
    $rootScope.active = false;
    $rootScope.event = {};

    $scope.checkStatus = function () {

        $rootScope.loadingActive = true;

        EventService.getActive().then(function (response) {
            $rootScope.loadingActive = false;

            if (!response.data.data || response.data.data === undefined) {
                $scope.active = false;
            } else {

                if (response.data.data.custom_fields.status === "active") {

                  $scope.active = true;
                  $scope.event = response.data.data;

                } else {

                  $scope.active = false;

                }

            }


        }, function (err) {

            $rootScope.loadingActive = false;
            $scope.active = false;

        });

    };

    $scope.checkStatus();


}]);

'use strict';

angular.module('inverroy').controller('ProfileController', ['$scope', 'UserService', 'AuthService', '$state', function ($scope, UserService, AuthService, $state) {

    AuthService.checkUser();


    if ($scope.isLogged) {

        $state.go('dashboard.profile');

    } else {

        $state.go('login');

    }

    // Variables
    $scope.message = {};
    $scope.updatingPassword = false;

    $scope.updatePassword = function (user) {

        $scope.updatingPassword = true;

        UserService.updatePassword(user).then(function (response) {

            $scope.updatingPassword = false;

            $scope.showMessage('success', response.data.message);

        }, function (err) {

            $scope.updatingPassword = false;

            $scope.showMessage('danger', response.data.message);

        });

    };

    $scope.updateProfile = function (profile) {

        $scope.updating = true;

        UserService.update(profile.id, profile).then(function (response) {

            $scope.updating = false;
            $scope.profile = response.data.data;

            $scope.showMessage('success', response.data.message);


        }, function (err) {

            $scope.updating = false;

            $scope.showMessage('danger', response.data.message);

        });

    };


}]);

'use strict';

angular.module('inverroy').controller('RequestController', [
    '$scope',
    'EventService',
    'AuthService',
    '$state',
    'Pagination',
    'RequestService',
    'FileService',

    function ($scope, EventService, AuthService, $state, Pagination, RequestService, FileService) {


    AuthService.checkUser();

    if ($scope.isLogged) {

      $state.go('dashboard.requests');

    } else {

      $state.go('login');

    }

    //Variables
    $scope.requests = [];
    $scope.file = {};
    $scope.loadingRequests = true;
    $scope.pagination = Pagination.getNew(15);

    // Get all requests
    function getAll() {

      RequestService.all().then(function (response) {

        $scope.loadingRequests = false;
        $scope.requests = response.data.data;

        if ($scope.requests) {
            $scope.pagination.numPages = Math.ceil($scope.requests.length/$scope.pagination.perPage);
        }


      }, function (err) {

        $scope.loadingRequests = false;
        $scope.showMessage('danger', err.data.message.reason);

      });

    }

    $scope.openModal = function (id) {

      $scope.file = {};

      FileService.download(id).then(function (response) {

          // window.open(response.data.data.url, '_blank');
          $scope.file = response.data.data;

      }, function (err) {

        $scope.showMessage('danger', err.data.message.reason);

      });

      $('#openFileModal').modal('show');

    };

    $scope.closeModal = function () {

      $scope.file = {};
      $('#openFileModal').modal('hide');

    };

    $scope.openFile = function () {

      window.open($scope.file.url, '_blank');
      $('#openFileModal').modal('hide');

    };

    // Run functions
    getAll();

}]);

'use strict';

angular.module('inverroy').controller('UsersController', ['$scope', 'UserService', 'AuthService', '$state', 'Pagination',  function ($scope, UserService, AuthService, $state, Pagination) {


    AuthService.checkUser();

    if ($scope.profile.role == "Admin" || $scope.profile.role == "Super Admin") {

        if ($scope.isLogged) {

            $state.go('dashboard.users');

        } else {

            $state.go('dashboard');

        }

    } else {

        $state.go('login');

    }

    //Variables
    $scope.users = [];
    $scope.user = {};
    $scope.newuser = {};
    $scope.loadingUsers = true;
    $scope.pagination = Pagination.getNew(15);
    $scope.sortType = 'user.first_name';
    $scope.sortReverse = false;

    // Functions
    function loadUsers() {

        UserService.getAll().then(function (response) {

            $scope.loadingUsers = false;
            $scope.users = response.data.data;

            if ($scope.users) {
              $scope.pagination.numPages = Math.ceil($scope.users.length/$scope.pagination.perPage);
            }


        }, function (err) {

            $scope.loadingUsers = false;
            $scope.showMessage('danger', err.data.message.reason);

        });

    }

    $scope.changeIcon = function(id) {

        var id = '#span_' + id;

        var icon = $(id);

        if (icon.hasClass('glyphicon-menu-right')) {

            icon.removeClass('glyphicon-menu-right');
            icon.addClass('glyphicon-menu-down');

        } else if (icon.hasClass('glyphicon-menu-down')) {

            icon.removeClass('glyphicon-menu-down');
            icon.addClass('glyphicon-menu-right');

        }

    };


    $scope.deleteUser = function (id) {

      swal({
        title: 'Are you sure?',
        text: "The User will be removed",
        type: 'warning',
        showCancelButton: true,
        buttonsStyling: false,
        confirmButtonClass: 'btn button-blue margin-right',
        cancelButtonClass: 'btn button-red',
        confirmButtonText: 'Yes, remove it!'
      }).then(function() {
        $scope.removingUser = true;

        UserService.delete(id).then(function (response) {

            $scope.removingUser = false;
            loadUsers();

        }, function (err) {

            $scope.removingUser = false;
            $scope.showMessage('danger', err.data.message.reason);

        });
      })

    };


    $scope.createUser = function (user, form) {

        form.$setPristine();
        $scope.newuser = {};
        $scope.newuser.name         = '';
        $scope.newuser.department   = '';
        $scope.newuser.role         = '';
        $scope.newuser.crisis_role  = '';
        $scope.newuser.mobile_phone = '';
        $scope.newuser.work_phone   = '';
        $scope.newuser.work_phone   = '';
        $scope.newuser.home_phone   = '';
        $scope.newuser.email        = '';

        $scope.creatingUser = true;

        UserService.create(user).then(function (response) {

            $scope.creatingUser = false;
            $scope.showMessage('success', response.data.message);

            loadUsers();

        }, function (err) {

            $scope.creatingUser = false;
            $scope.showMessage('danger', err.data.message.reason);

        });

    };

    $scope.editUser = function (user) {

        $scope.user = user;
        $('#edituserModal').modal('show');

    };

    $scope.updateUser = function (user) {

        var userId = user.id;
        $scope.updatingUser = true;

        UserService.update(userId, user).then(function (response) {

            $scope.updatingUser = false;
            $scope.showMessage('success', response.data.message);

            $('#edituserModal').modal('hide');

        }, function (err) {

            $scope.updatingUser = true;
            $scope.showMessage('danger', err.data.message.reason);

        });

    };

    $scope.addUsers = function (file) {

        var objs = [];
        var users = [];
        var toCreate = [];

        for (var i = 0; i < file.length; i++) {
            objs.push(file[i]);
        }

        for (var obj in objs) {

            users.push(objs[obj][0].split(','));

        }

        users.shift();

        for (var user in users) {

            var newUser = {};
            newUser.name = users[user][0];
            newUser.department = users[user][1];
            newUser.role = users[user][2];
            newUser.crisis_role = users[user][3];
            newUser.mobile_phone = users[user][4];
            newUser.work_phone = users[user][5];
            newUser.home_phone = users[user][6];
            newUser.email = users[user][7];

            toCreate.push(newUser);

        }

        for (var newUser in toCreate) {

            $scope.createUser(toCreate[newUser]);

        }

    };


    loadUsers();

}]);

'use strict';

angular.module('inverroy').service('ActivityService', ['$http', function ($http) {

    this.createFeed = function (message) {

        return $http.post("api/activity", message);

    };

    this.getFeed = function (event_id) {

        return $http.get("api/activity/" + event_id);

    };

}]);

'use strict';

angular.module('inverroy').service('AuthService', ['$http', '$rootScope', '$state', function ($http, $rootScope, $state) {

    var isLogged = false;
    var profile  = {};

    this.login = function (user) {

        $rootScope.logging = true;

         $http.post('api/auth/login', user).then(function (response) {

            profile  = response.data.data;
            isLogged = true;

        }, function (err) {

            $rootScope.error = err.data;

        });

        $rootScope.logging = false;

    };

    this.checkUser = function () {

        $rootScope.checking = true;

        $http.get('api/auth/login').then(function (response) {

            if (response.data.type === "success") {

                profile  = response.data.data;
                isLogged = true;

            } else {

                isLogged = false;

            }

            $rootScope.checking = false;

        });

    };

    this.logout = function () {

        $http.get('api/auth/logout').then(function (response) {

            profile     = {};
            isLogged    = false;

        }, function (err) {

            console.log(err);

        });

    };

    this.setPassword = function (user) {

      return $http.get('api/auth/setPassword', user);

    };

    this.recover = function (email) {

        return $http.get('api/auth/recover/' + email);

    };

    this.currentUser = function () {

        return profile;

    };

    this.logged = function () {

        return isLogged;

    };


}]);

'use strict';

angular.module('inverroy').service('CheckService', ['$http', function ($http) {

    this.getCheks = function (event_id) {

        return $http.get("api/checks/" + event_id);

    };

    this.createCheck = function (event_id) {

        return $http.post("api/checks/" +  event_id);

    };

}]);

'use strict';

angular.module('inverroy').service('EmailService', ['$http', function ($http) {

    this.sendEmail = function (email) {

        return $http.post('api/email', email);

    };

    this.sendRequest = function (data) {

        return $http.post('api/email/request', data);

    };

    this.sendSummary = function (data) {

      return $http.post('api/email/summary', data);

    };

}]);

'use strict';

angular.module('inverroy').service('EventService', ['$http', function ($http) {

    this.getAll = function () {

        return $http.get("api/events");

    };

    this.createEvent = function (event) {

        return $http.post("api/events", event);

    };

    this.getActive = function () {

        return $http.get("api/events/getActive");

    };

    this.stopIncident = function (eventId) {

        return $http.put("api/events/" + eventId);

    };

    this.updateIncident = function (incident) {

        return $http.put("api/events", incident);

    };

    this.getOneEvent = function (id) {

        return $http.get('api/events/' + id);

    };

    this.remove = function (id) {

      return $http.delete('api/events/' + id);

    };

}]);

'use strict';

angular.module('inverroy').service('FileService', ['$http', function ($http) {

    this.getFiles = function () {

        return $http.get("api/files");

    };

    this.updateFile = function (file_id, file) {

        return $http.put("api/files/" + file_id, file);

    };

    this.deleteFile = function (file_id) {

        return $http.delete("api/files/" + file_id);

    };

    this.searchFile = function (file_id) {

        return $http.get("api/files/" + file_id);

    };

    this.download = function (eventId) {

      return $http.get('api/files/pdf/' + eventId);

    };

}]);

'use strict';

angular.module('inverroy').service('LocationService', ['$http', function ($http) {

    this.create = function (location) {

        return $http.post("api/locations", location);

    };

    this.getPlace = function (placeId) {

        return $http.get("api/locations/" + placeId);

    };

    this.getAll = function () {

        return $http.get("api/locations");

    };

    this.deleteLocation = function (placeId) {

        return $http.delete("api/locations/" + placeId);

    };

    this.updateLocation = function (placeId, location) {

        return $http.put("api/locations/" + placeId, location);

    };

}]);

'use strict';

angular.module('inverroy').service('NotificationService', ['$http', function ($http) {

    this.pushNotification = function (message) {

        return $http.post('api/notifications', message);

    };

}]);

'use strict';

angular.module('inverroy').service('PdfService', ['$http', function ($http) {

    this.createPdf = function (data) {

        return $http.post('api/pdf', data);

    };

}]);

'use strict';

angular.module('inverroy').service('RequestService', ['$http', function ($http) {

  this.all = function () {

    return $http.get('api/requests');

  };


}]);

'use strict';

angular.module('inverroy').service('UserService', ['$http', function ($http) {

    this.getAll = function () {

        return $http.get('api/users');

    };

    this.create = function (user) {

        return $http.post('api/users', user);

    };

    this.search = function (userId) {

        return $http.get('api/users/' + userId);

    };

    this.update = function (userId, user) {

        return $http.put('api/users/' + userId, user);

    };

    this.updateProfile = function (user) {

        return $http.put('api/users/editProfile', user);

    };

    this.delete = function (userId) {

        return $http.delete('api/users/' + userId);

    };

    this.updatePassword = function (user) {

        return $http.put('api/users', user);

    };

    this.getRole = function (role) {

        return $http.get('api/users/getRole/' + role);

    };


}]);
