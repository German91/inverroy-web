'use strict';

angular.module('inverroy').service('EmailService', ['$http', function ($http) {

    this.sendEmail = function (email) {

        return $http.post('api/email', email);

    };

    this.sendRequest = function (data) {

        return $http.post('api/email/request', data);

    };

    this.sendSummary = function (data) {

      return $http.post('api/email/summary', data);

    };

}]);
