'use strict';

angular.module('inverroy').service('RequestService', ['$http', function ($http) {

  this.all = function () {

    return $http.get('api/requests');

  };


}]);
