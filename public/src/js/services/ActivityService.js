'use strict';

angular.module('inverroy').service('ActivityService', ['$http', function ($http) {

    this.createFeed = function (message) {

        return $http.post("api/activity", message);

    };

    this.getFeed = function (event_id) {

        return $http.get("api/activity/" + event_id);

    };

}]);
