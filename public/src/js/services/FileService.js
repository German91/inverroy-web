'use strict';

angular.module('inverroy').service('FileService', ['$http', function ($http) {

    this.getFiles = function () {

        return $http.get("api/files");

    };

    this.updateFile = function (file_id, file) {

        return $http.put("api/files/" + file_id, file);

    };

    this.deleteFile = function (file_id) {

        return $http.delete("api/files/" + file_id);

    };

    this.searchFile = function (file_id) {

        return $http.get("api/files/" + file_id);

    };

    this.download = function (eventId) {

      return $http.get('api/files/pdf/' + eventId);

    };

}]);
