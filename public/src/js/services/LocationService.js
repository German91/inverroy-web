'use strict';

angular.module('inverroy').service('LocationService', ['$http', function ($http) {

    this.create = function (location) {

        return $http.post("api/locations", location);

    };

    this.getPlace = function (placeId) {

        return $http.get("api/locations/" + placeId);

    };

    this.getAll = function () {

        return $http.get("api/locations");

    };

    this.deleteLocation = function (placeId) {

        return $http.delete("api/locations/" + placeId);

    };

    this.updateLocation = function (placeId, location) {

        return $http.put("api/locations/" + placeId, location);

    };

}]);
