'use strict';

angular.module('inverroy').service('UserService', ['$http', function ($http) {

    this.getAll = function () {

        return $http.get('api/users');

    };

    this.create = function (user) {

        return $http.post('api/users', user);

    };

    this.search = function (userId) {

        return $http.get('api/users/' + userId);

    };

    this.update = function (userId, user) {

        return $http.put('api/users/' + userId, user);

    };

    this.updateProfile = function (user) {

        return $http.put('api/users/editProfile', user);

    };

    this.delete = function (userId) {

        return $http.delete('api/users/' + userId);

    };

    this.updatePassword = function (user) {

        return $http.put('api/users', user);

    };

    this.getRole = function (role) {

        return $http.get('api/users/getRole/' + role);

    };


}]);
