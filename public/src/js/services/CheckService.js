'use strict';

angular.module('inverroy').service('CheckService', ['$http', function ($http) {

    this.getCheks = function (event_id) {

        return $http.get("api/checks/" + event_id);

    };

    this.createCheck = function (event_id) {

        return $http.post("api/checks/" +  event_id);

    };

}]);
