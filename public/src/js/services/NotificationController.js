'use strict';

angular.module('inverroy').service('NotificationService', ['$http', function ($http) {

    this.pushNotification = function (message) {

        return $http.post('api/notifications', message);

    };

}]);
