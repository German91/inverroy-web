'use strict';

angular.module('inverroy').service('EventService', ['$http', function ($http) {

    this.getAll = function () {

        return $http.get("api/events");

    };

    this.createEvent = function (event) {

        return $http.post("api/events", event);

    };

    this.getActive = function () {

        return $http.get("api/events/getActive");

    };

    this.stopIncident = function (eventId) {

        return $http.put("api/events/" + eventId);

    };

    this.updateIncident = function (incident) {

        return $http.put("api/events", incident);

    };

    this.getOneEvent = function (id) {

        return $http.get('api/events/' + id);

    };

    this.remove = function (id) {

      return $http.delete('api/events/' + id);

    };

}]);
