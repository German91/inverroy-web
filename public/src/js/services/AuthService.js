'use strict';

angular.module('inverroy').service('AuthService', ['$http', '$rootScope', '$state', function ($http, $rootScope, $state) {

    var isLogged = false;
    var profile  = {};

    this.login = function (user) {

        $rootScope.logging = true;

         $http.post('api/auth/login', user).then(function (response) {

            profile  = response.data.data;
            isLogged = true;

        }, function (err) {

            $rootScope.error = err.data;

        });

        $rootScope.logging = false;

    };

    this.checkUser = function () {

        $rootScope.checking = true;

        $http.get('api/auth/login').then(function (response) {

            if (response.data.type === "success") {

                profile  = response.data.data;
                isLogged = true;

            } else {

                isLogged = false;

            }

            $rootScope.checking = false;

        });

    };

    this.logout = function () {

        $http.get('api/auth/logout').then(function (response) {

            profile     = {};
            isLogged    = false;

        }, function (err) {

            console.log(err);

        });

    };

    this.setPassword = function (user) {

      return $http.get('api/auth/setPassword', user);

    };

    this.recover = function (email) {

        return $http.get('api/auth/recover/' + email);

    };

    this.currentUser = function () {

        return profile;

    };

    this.logged = function () {

        return isLogged;

    };


}]);
