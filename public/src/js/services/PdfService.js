'use strict';

angular.module('inverroy').service('PdfService', ['$http', function ($http) {

    this.createPdf = function (data) {

        return $http.post('api/pdf', data);

    };

}]);
