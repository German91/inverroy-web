'use strict';

angular.module('inverroy', [
  'ui.router',
  'angular-spinkit',
  'jcs-autoValidate',
  'ngFileUpload',
  'uiGmapgoogle-maps',
  'angularReverseGeocode',
  'vsGoogleAutocomplete',
  'simplePagination',
  'ngCsvImport',
  'htmlToPdfSave',
  'ngProgress'])
.run([
  'bootstrap3ElementModifier',
  function (bootstrap3ElementModifier) {
        bootstrap3ElementModifier.enableValidationStateIcons(true);
 }])
.config(function($stateProvider, $urlRouterProvider) {

  $stateProvider
    .state('login', {
      url: '/login',
      views: {

        'content' : {
          templateUrl : 'views/auth/login.html',
          controller  : 'AuthController'
        }

      }
  })
  .state('recover', {
      url: '/recover',
      views: {

          'content' : {
              templateUrl : 'views/auth/recover.html',
              controller  : 'AuthController'
          }

      }
  })
  .state('reset', {
    url: '/resetPassword:reset_password_token',
    views: {

      'content@' : {
        templateUrl: 'views/auth/reset.html',
        controller: 'AuthController'
      }

    }
  })
  .state('confirm', {
    url: '/confirmAccount',
    views: {

      'content@' : {
        templateUrl: 'views/auth/confirm.html',
        controller: 'AuthController'
      }

    }
  })
  .state('dashboard', {
      url: '/',
      views: {

          'header'  : {
              templateUrl : 'views/app/header.html'
          },

          'content' : {
              templateUrl : 'views/dashboard/index.html',
              controller  : 'DashboardController'
          }

      }
  })
  .state('dashboard.profile', {
      url: 'profile',
      views: {

          'content@' : {
              templateUrl : 'views/dashboard/profile.html',
              controller  : 'ProfileController'
          }

      }
  })
  .state('dashboard.users', {
      url: 'users',
      views: {

          'content@' : {
              templateUrl : 'views/dashboard/users.html',
              controller  : 'UsersController'
          }

      }
  })
  .state('dashboard.locations', {
      url: 'locations',
      views: {

          'content@' : {
              templateUrl : 'views/dashboard/locations.html',
              controller  : 'LocationsController'
          }

      }
  })
  .state('dashboard.incidents', {
      url: 'incidents',
      views: {

          'content@' : {
              templateUrl : 'views/dashboard/incidents.html',
              controller  : 'IncidentController'
          }

      }
  })
  .state('dashboard.documents', {
      url: 'documents',
      views: {

          'content@' : {
              templateUrl : 'views/dashboard/documents.html',
              controller  : 'DocumentsController'
          }

      }
  })
  .state('dashboard.requests', {
    url: 'requests',
    views: {

      'content@' : {
        templateUrl: 'views/dashboard/requests.html',
        controller: 'RequestController'
      }

    }
  });


  $urlRouterProvider.otherwise('/login');

});
