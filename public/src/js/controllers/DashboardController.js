'use strict';

angular.module('inverroy').controller('DashboardController',
['$scope', 'AuthService', 'UserService', '$state', '$timeout', 'EventService', 'LocationService', 'CheckService', 'Pagination', 'NotificationService', 'ActivityService', 'PdfService', '$sce', 'FileService', 'Upload', 'EmailService', '$filter', 'ngProgressFactory',
function ($scope, AuthService, UserService, $state, $timeout, EventService, LocationService, CheckService, Pagination, NotificationService, ActivityService, PdfService, $sce, FileService, Upload, EmailService, $filter, ngProgressFactory) {

    $('body').removeClass('loginBackground');
    $('body').addClass('bg-content');

    AuthService.checkUser();


    if ($scope.isLogged) {

        $state.go('dashboard');

    } else {

        $state.go('login');

    }

    // Variables
    $scope.locations = [];
    $scope.users = [];
    $scope.numUsers = 15;
    $scope.pagination = Pagination.getNew($scope.numUsers);
    $scope.pagination2 = Pagination.getNew($scope.numUsers);
    $scope.locations = [];
    $scope.incident = {};
    $scope.toSend = {};
    $scope.toSend.ids = "";
    $scope.toSend.emails = "";
    $scope.incident.location = {};
    $scope.showButton = false;
    $scope.feeds = [];
    $scope.noShow = true;
    $scope.mess = {};
    $scope.sortType = 'user.first_name';
    $scope.sortReverse = false;
    $scope.note = {};
    $scope.emails = [];
    $scope.email = {};
    $scope.summaryText = {};
    $scope.progressbar = ngProgressFactory.createInstance();

    // Functions
    function getFeed() {

        $scope.loadingFeed = true;

        ActivityService.getFeed($scope.event.id).then(function (response) {
            $scope.loadingFeed = false;
            $scope.feeds = response.data.data;


            if ($scope.feeds) {
              $scope.pagination2.numPages = Math.ceil($scope.feeds.length/$scope.pagination2.perPage);
            }


        }, function (err) {

            $scope.loadingFeed = false;

        });

    }

    function checkEvent() {

        if ($scope.active) {

            EventService.getActive().then(function (response) {

                if (response.data.data.id || response.data.data.id !== undefined) {

                    var event_id = response.data.data.id;
                    getFeed();
                    $scope.checking = true;

                    CheckService.getCheks(event_id).then(function (response) {

                        $scope.users = [];

                        $scope.checking = false;
                        $scope.users = response.data.data;

                        if ($scope.users) {
                           $scope.pagination.numPages = Math.ceil($scope.users.length/$scope.pagination.perPage);
                        }


                    }, function (err) {

                        $scope.showMessage('danger', err.data.message.reason);
                        $scope.checking = false;

                    });

                }

            }, function (err) {

                $scope.showMessage('danger', err.data.message.reason);

            });

        }

    }

    function searchUsers() {

        $scope.users = [];

        $scope.checking = true;
        $scope.loadingFeed = true;

        setInterval(function () {

            var array = [];

            EventService.getActive().then(function (response) {

                if (response.data.data) {

                  if (response.data.data.id) {

                    var event_id = response.data.data.id;

                       ActivityService.getFeed($scope.event.id).then(function (response) {
                        $scope.feeds = response.data.data;

                        if ($scope.feeds) {
                          $scope.pagination2.numPages = Math.ceil($scope.feeds.length/$scope.pagination2.perPage);
                        }

                        $scope.loadingFeed = false;
                    });

                    CheckService.getCheks(event_id).then(function (response) {

                        $scope.users = [];

                        array = response.data.data;
                        $scope.checking = false;

                        if (array) {
                          if (array.length > $scope.users.length) {

                              $scope.users = array;

                              if ($scope.users) {
                                $scope.pagination.numPages = Math.ceil($scope.users.length/$scope.pagination.perPage);
                              }

                          }

                        }

                    }, function (err) {

                        $scope.checking = false;
                        $scope.showMessage('danger', err.data.message.reason);

                    });

                  }

                }

            }, function (err) {

                $scope.showMessage('danger', err.data.message.reason);

            });
        }, 5000);

    }

    function initialize() {

        $scope.geocoder = new google.maps.Geocoder();

        $scope.lat = 55.93334052458581;
        $scope.lon = -3.2139009644180305;
        $scope.map = {center: {latitude: 55.93334052458581, longitude: -3.2139009644180305 }, zoom: 14, control: {} };
        $scope.options = {scrollwheel: false};
        $scope.marker = {
          id: 0,
          coords: {
            latitude: 55.93334052458581,
            longitude: -3.2139009644180305
          },
          options: { draggable: true },
          events: {
            dragend: function (marker, eventName, args) {

              $scope.lat = marker.getPosition().lat();
              $scope.lon = marker.getPosition().lng();

              var latlng = new google.maps.LatLng($scope.lat, $scope.lon);

              $scope.geocoder.geocode({ 'latLng': latlng }, function (results, status) {

                $scope.incident.location.address = results[0].formatted_address;
                $scope.incident.location.latitude = $scope.lat;
                $scope.incident.location.longitude = $scope.lon;

                for (var address in results[0].address_components) {

                    if (results[0].address_components[address].types[0] === "country") {

                        $scope.incident.location.country =  results[0].address_components[address].long_name;

                    }

                }

              });

              $scope.marker.options = {
                draggable: true,
                labelContent: $scope.incident.location.address,
                labelAnchor: "100 0",
                labelClass: "marker-labels"
              };
            }
          }
        };

    }


    function getLocations() {

        $scope.loadinglocations = true;

        LocationService.getAll().then(function (response) {

            $scope.loadinglocations = false;
            $scope.locations = response.data.data;

        }, function (err) {

            $scope.loadinglocations = false;
            $scope.showMessage('danger', err.data.message.reason);

        });

    }


    $scope.changeNumUsers = function (num) {

      $scope.numUsers = num;
      $scope.pagination = Pagination.getNew($scope.numUsers);

    };


    $scope.getAddress = function (address) {

        $scope.geocoder.geocode({ 'address': address }, function (results, status) {

            if (status === google.maps.GeocoderStatus.OK) {

                $scope.newcoords = results[0].geometry.location;

                $scope.marker.coords.latitude = $scope.newcoords.lat();
                $scope.marker.coords.longitude = $scope.newcoords.lng();

                $scope.map.center.latitude = $scope.newcoords.lat();
                $scope.map.center.longitude = $scope.newcoords.lng();

                $scope.incident.location.address = results[0].formatted_address;
                $scope.incident.location.latitude = $scope.newcoords.lat();
                $scope.incident.location.longitude = $scope.newcoords.lng();

                $scope.$apply();

                for (var address in results[0].address_components) {

                    if (results[0].address_components[address].types[0] === "country") {

                        $scope.incident.location.country =  results[0].address_components[address].long_name;

                    }

                }
            }

        });

    };

    $scope.changeIcon = function(id) {

        var id = '#span_' + id;

        var icon = $(id);

        if (icon.hasClass('glyphicon-menu-right')) {

            icon.removeClass('glyphicon-menu-right');
            icon.addClass('glyphicon-menu-down');

        } else if (icon.hasClass('glyphicon-menu-down')) {

            icon.removeClass('glyphicon-menu-down');
            icon.addClass('glyphicon-menu-right');

        }

    };

    $scope.createIncident = function (incident) {

        var newIncident = {};
        $scope.creatingincident = true;

        if (!incident.selected || incident.selected === null) {

            newIncident.hasTemporary = true;

            newIncident.name                = incident.name;
            newIncident.description         = incident.description;
            newIncident.phone               = incident.phone;
            newIncident.temporary_name      = incident.location.address;
            newIncident.temporary_address   = incident.location.address;
            newIncident.temporary_country   = incident.location.country;
            newIncident.temporary_latitude  = incident.location.latitude;
            newIncident.temporary_longitude = incident.location.longitude;

            EventService.createEvent(newIncident).then(function (response) {

                $scope.creatingincident = false;
                $scope.showMessage('success', response.data.message);

                $scope.checkStatus();
                checkEvent();
                getLocations();

            }, function (err) {

                $scope.creatingincident = false;
                $scope.showMessage('danger', err.data.message.reason);

            });

        } else {

            incident.place_id = incident.selected;

            EventService.createEvent(incident).then(function (response) {

                $scope.creatingincident = false;
                $scope.showMessage('success', response.data.message);
                $scope.checkStatus();

            }, function (err) {

                $scope.creatingincident = false;
                $scope.showMessage('danger', err.data.message.reason);

            });

        }

    };

    $scope.getAddress2 = function (address) {

        $scope.geocoder.geocode({ 'address': address }, function (results, status) {

            if (status === google.maps.GeocoderStatus.OK) {

                $scope.newcoords = results[0].geometry.location;

                $scope.marker2.coords.latitude = $scope.newcoords.lat();
                $scope.marker2.coords.longitude = $scope.newcoords.lng();

                $scope.map2.center.latitude = $scope.newcoords.lat();
                $scope.map2.center.longitude = $scope.newcoords.lng();

                if (!$scope.event.custom_fields.hasTemporary && $scope.currentEvent.place.id) {

                    $scope.event.place.address = results[0].formatted_address;
                    $scope.event.place.latitude = $scope.newcoords.lat();
                    $scope.event.place.longitude = $scope.newcoords.lng();

                    for (var address in results[0].address_components) {

                        if (results[0].address_components[address].types[0] === "country") {

                            $scope.event.place.country =  results[0].address_components[address].long_name;

                        }

                    }

                } else {

                    $scope.newplace.address =  results[0].formatted_address;
                    $scope.newplace.latitude = $scope.newcoords.lat();
                    $scope.newplace.longitude = $scope.newcoords.lng();

                    for (var address in results[0].address_components) {

                        if (results[0].address_components[address].types[0] === "country") {

                            $scope.newplace.country =  results[0].address_components[address].long_name;

                        }

                    }

                }

                $scope.$apply();
            }

        });

    };

    $scope.stopIncident = function () {

        EventService.stopIncident($scope.event.id).then(function (response) {

            $scope.showMessage('success', response.data.message);
            $scope.checkStatus();
            $('#stopModal').modal('hide');

        }, function (err) {

            $scope.showMessage('danger', err.data.message.reason);
            $('#stopModal').modal('hide');

        });

    };

    $scope.updateIncident = function (incident) {

        $scope.updatingevent = true;
        $scope.toUpdate = {};

        $scope.toUpdate.event_id = incident.id;
        $scope.toUpdate.name = incident.name;
        $scope.toUpdate.phone = incident.custom_fields.phone;
        $scope.toUpdate.details = incident.details;

        if (incident.place && incident.place.id) {

            $scope.toUpdate.hasTemporary = false;
            $scope.toUpdate.place_id = incident.place.id;

        } else {
            $scope.toUpdate.hasTemporary = true;

            $scope.toUpdate.temporary_name      = $scope.newplace.address;
            $scope.toUpdate.temporary_address   = $scope.newplace.address;
            $scope.toUpdate.temporary_country   = $scope.newplace.country;
            $scope.toUpdate.temporary_latitude  = $scope.newplace.latitude;
            $scope.toUpdate.temporary_longitude = $scope.newplace.longitude;

        }

        EventService.updateIncident($scope.toUpdate).then(function (response) {

            $scope.updatingevent = false;
            $scope.showMessage('success', response.data.message);

            $scope.checkStatus();
            $('#editincidentModal').modal('hide');

        }, function (err) {

            $scope.updatingevent = false;
            $scope.showMessage('danger', err.data.message.reason);

        });

    };

    $scope.editIncident = function () {

        $scope.showMap = false;
        $scope.currentEvent = {};
        $scope.newplace = {};
        $scope.loadingEvent = true;
        $scope.loadingActive = true;

        if ($scope.event.custom_fields.hasTemporary) {

          $scope.newplace.address   = $scope.event.custom_fields.temporary_address;
          $scope.newplace.latitude  = $scope.event.custom_fields.temporary_latitude;
          $scope.newplace.longitude = $scope.event.custom_fields.temporary_longitude;
          $scope.newplace.country   = $scope.event.custom_fields.temporary_country;
          $scope.newplace.name      = $scope.event.custom_fields.temporary_name;

        }

        EventService.getActive().then(function (response) {
            $scope.loadingEvent = false;
            $scope.currentEvent = response.data.data;
            $scope.loadingActive = false;

            if ($scope.currentEvent.custom_fields.hasTemporary) {

                if ($scope.currentEvent.place) {

                    $scope.currentEvent.place = "";

                }

            }

            $('#editincidentModal').modal('show');

                if ($scope.currentEvent.custom_fields.hasTemporary) {

                    $timeout(function () {
                        $scope.map2 = {};
                        $scope.marker2 = {};
                        $scope.showMap = true;
                        $scope.map2 = {center: {latitude: $scope.currentEvent.custom_fields.temporary_latitude, longitude: $scope.currentEvent.custom_fields.temporary_longitude }, zoom: 14, control: {} };
                        $scope.marker2 = {
                          id: 0,
                          coords: {
                            latitude: $scope.currentEvent.custom_fields.temporary_latitude,
                            longitude: $scope.currentEvent.custom_fields.temporary_longitude
                          },
                          options: { draggable: true },
                          events: {
                            dragend: function (marker2, eventName, args) {

                              $scope.lat = marker2.getPosition().lat();
                              $scope.lon = marker2.getPosition().lng();

                              var latlng = new google.maps.LatLng($scope.lat, $scope.lon);

                              $scope.geocoder.geocode({ 'latLng': latlng }, function (results, status) {

                                $scope.newplace.address = results[0].formatted_address;

                                for (var address in results[0].address_components) {

                                    if (results[0].address_components[address].types[0] === "country") {

                                        $scope.newplace.country =  results[0].address_components[address].long_name;

                                    }

                                }


                              });

                              $scope.marker2.options = {
                                draggable: true,
                                labelContent: $scope.newplace.address,
                                labelAnchor: "100 0",
                                labelClass: "marker-labels"
                              };
                            }
                          }
                        };

                    }, 1000);


                } else {

                    $timeout(function () {
                        $scope.map2 = {};
                        $scope.marker2 = {};
                        $scope.showMap = true;
                        $scope.map2 = {center: {latitude: $scope.currentEvent.place.latitude, longitude: $scope.currentEvent.place.longitude }, zoom: 14, control: {} };
                        $scope.marker2 = {
                          id: 0,
                          coords: {
                            latitude: $scope.currentEvent.place.latitude,
                            longitude:  $scope.currentEvent.place.longitude
                          },
                          options: { draggable: true },
                          events: {
                            dragend: function (marker2, eventName, args) {

                              $scope.lat = marker2.getPosition().lat();
                              $scope.lon = marker2.getPosition().lng();

                              var latlng = new google.maps.LatLng($scope.lat, $scope.lon);

                              $scope.geocoder.geocode({ 'latLng': latlng }, function (results, status) {

                                $scope.newplace.address = results[0].formatted_address;

                                for (var address in results[0].address_components) {

                                    if (results[0].address_components[address].types[0] === "country") {

                                        $scope.newplace.country =  results[0].address_components[address].long_name;

                                    }

                                }


                              });

                              $scope.marker2.options = {
                                draggable: true,
                                labelContent: $scope.newplace.address,
                                labelAnchor: "100 0",
                                labelClass: "marker-labels"
                              };
                            }
                          }
                        };

                    }, 1000);


                }

        }, function (err) {

            $scope.loadingEvent = false;
            $scope.showMessage('danger', err.data.message.reason);

        });

    };


    $scope.stopModal = function () {

        $('#stopModal').modal('show');

    };

    $scope.closeStop = function () {

        $('#stopModal').modal('hide');

    };

    function getRoleEmail(role) {

        for (var user in $scope.users) {

          if ($scope.users[user].user.custom_fields.crisis_role === role) {

              if ($scope.users[user].user.email !== undefined) {
                $scope.toSend.emails += $scope.users[user].user.email + ',';
              }

          }

        }

    }

    $scope.summary = function (summaryData, form) {

      if ($scope.email.newEmail !== '') {
        $scope.emails.push($scope.email.newEmail);
      }

      $scope.sendingSummary = true;
      $scope.progressbar.start();

      $.notify({

        message: 'We are creating the summary, please do not close that page until the progress be finished.'

      }, {

        type: 'warning',
        animate: {
          enter: 'animated fadeInRight',
          exit: 'animated fadeOutRight'
        },
        offset: {
          x: 50,
          y: 100
        },
        delay: 16000

      });

      form.$setPristine();
      $scope.summaryText        = {};
      $scope.summaryText.text   = '';
      $scope.summaryText.member = false;
      $scope.summaryText.cmt    = false;
      $scope.summaryText.chair  = false;
      $scope.summaryText.other  = false;

      $('#sendSummaryModal').modal('hide');

        var users = {

            usersEmail:     [],
            usersDept:      [],
            usersMobile:    [],
            usersWork:      [],
            usersHome:      [],
            usersMessage:   [],

        };

        var feeds = {

          feedsType: [],
          feedsTime: [],
          feedsWho: [],
          feedsDescription: []

        };

        var incident = {};

        incident.details = $scope.event.details;

        if (!$scope.event.custom_fields.hasTemporary) {

          incident.name     = $scope.event.name;
          incident.address  = $scope.event.place.address;
          incident.country  = $scope.event.place.country;

        } else {

          incident.name     = ' ';
          incident.address  = $scope.event.custom_fields.temporary_address;
          incident.country  = $scope.event.custom_fields.temporary_country;

        }

        for (var user in $scope.users) {

            users.usersEmail.push($scope.users[user].user.email);
            users.usersDept.push($scope.users[user].user.custom_fields.department);
            users.usersMobile.push($scope.users[user].user.custom_fields.mobile_phone);
            users.usersWork.push($scope.users[user].user.custom_fields.work_phone);
            users.usersHome.push($scope.users[user].user.custom_fields.home_phone);
            users.usersMessage.push($scope.users[user].message);

        }

        function generateFeed () {

          var feeds = [];

          if ($scope.feeds.length > 0) {

            for (var feed in $scope.feeds) {

              $scope.feeds[feed].time = $filter('date')($scope.feeds[feed].time, "dd/MM/yyyy h:mm a");

              feeds.push(
                { text: $scope.feeds[feed].time },
                { text: 'To: ' + $scope.feeds[feed].who },
                ' ',
                { text: $scope.feeds[feed].text },
                ' '
              );

            }

          }

          return feeds;

        }

        for (var user in users.usersEmail) {

          if (users.usersEmail[user] === undefined) {
            users.usersEmail[user] = 'none';
          }

        }

        for (var user in users.usersDept) {

          if (users.usersDept[user] === undefined) {
            users.usersDept[user] = 'none';
          }

        }

        for (var user in users.usersMobile) {

          if (users.usersMobile[user] === undefined) {
            users.usersMobile[user] = 'none';
          }

        }

        for (var user in users.usersWork) {

          if (users.usersWork[user] === undefined) {
            users.usersWork[user] = 'none';
          }

        }

        for (var user in users.usersHome) {

          if (users.usersHome[user] === undefined) {
            users.usersHome[user] = 'none';
          }

        }

        for (var user in users.usersMessage) {

          if (users.usersMessage[user] === undefined) {
            users.usersMessage[user] = 'none';
          }

        }


        var table = {
            pageOrientation: 'landscape',
            content: [
              { text: incident.name },
              { text: incident.address },
              { text: incident.country },
              ' ',
              incident.details,
              ' ',
              { text: 'Who is Available', style: 'header' },
              ' ',
                {
                    table: {
                        headerRows: 1,
                        widths: [180, 50, 100, 100, 100, 100],
                        body: [
                            ['Email', 'Dept', 'Mobile', 'Work', 'Home', 'Availability'],
                            [users.usersEmail, users.usersDept, users.usersMobile, users.usersWork, users.usersHome, users.usersMessage]
                        ]
                    },
                    layout: 'lightHorizontalLines',
                    pageBreak: 'after'
                },
                ' ',
                { text: 'Activity Feed', style: 'header' },
                ' ',
                generateFeed()
            ],

        };

        pdfMake.createPdf(table).getBase64(function(dataURL) {

          var file = {
            data: dataURL,
            eventId: $scope.event.id,
            email: $scope.profile.email
          };


          PdfService.createPdf(file).then(function (response) {

            var file_id = response.data.data.id;

            FileService.searchFile(file_id).then(function (response) {

                var sentTo = "";

                if (summaryData.cmt) {

                    var role = "CMT";
                    sentTo += "CMT" + ', ';
                    getRoleEmail(role);

                }

                if (summaryData.member) {

                    var role = "BRTMember";
                    sentTo += "BRTMember" + ', ';
                    getRoleEmail(role);

                }

                if (summaryData.chair) {

                    var role = "BRTChair";
                    sentTo += "BRTChair" + ', ';
                    getRoleEmail(role);

                }

                if (summaryData.other) {

                    var role = "other";
                    sentTo += "other" + ', ';
                    getRoleEmail(role);

                }

                if ($scope.emails.length > 0) {

                  for (var email in $scope.emails) {

                    if ($scope.emails[email] !== undefined) {
                      $scope.toSend.emails += $scope.emails[email] + ', ';
                    }

                  }

                }

                var data = {
                  file: response.data.data.id,
                  message: summaryData.message,
                  who: $scope.toSend.emails
                };

                $scope.emails = [];


                EmailService.sendSummary(data).then(function (response) {

                  $scope.showMessage('success', 'Created summary successful');
                  $scope.sendingSummary = false;
                  $scope.progressbar.complete();

                }, function (err) {

                  $scope.sendingSummary = false;
                  $scope.progressbar.complete();
                  $scope.showMessage('danger', err.data.message.reason);

                });


            });

          }, function (err) {

            $scope.showMessage('danger', err.data.message.reason);

          });

        });


    };

    function getRole(role) {

        for (var user in $scope.users) {

          if ($scope.users[user].user.custom_fields.crisis_role === role) {

              $scope.toSend.ids += $scope.users[user].user.id + ',';

          }

        }

    }


    $scope.sendMessage = function (mess, form) {

      form.$setPristine();
      $scope.mess = {};
      $scope.toSend.text = mess.text;
      $scope.toSend.ids = '';

        var sentTo = "";

        if (mess.cmt) {

            var role = "CMT";
            sentTo += "CMT" + ', ';
            getRole(role);

        }

        if (mess.member) {

            var role = "BRTMember";
            sentTo += "BRTMember" + ', ';
            getRole(role);

        }

        if (mess.chair) {

            var role = "BRTChair";
            sentTo += "BRTChair" + ', ';
            getRole(role);

        }

        if (mess.other) {

            var role = "other";
            sentTo += "other" + ', ';
            getRole(role);

        }


        if ($scope.toSend.ids === "") {

          $scope.showMessage('danger', 'No Active Users - There are currently no users available. This message will be added to the activity feed and users will receive this information if they become available');

           var message = {};

           message.type = "notification";
           message.time = new Date();
           message.who = sentTo;
           message.message = $scope.toSend.text;
           message.id = $scope.profile.id;
           message.event = $scope.event.id;

          ActivityService.createFeed(message).then(function (response){

            getFeed();

          });

        } else {


          NotificationService.pushNotification($scope.toSend).then(function (response) {

             $scope.sending = false;
             $scope.showMessage('success', response.data.message);

             var message = {};

             message.type = "notification";
             message.time = new Date();
             message.who = sentTo;
             message.message = $scope.toSend.text;
             message.id = $scope.profile.id;
             message.event = $scope.event.id;

             ActivityService.createFeed(message).then(function (response){

                 getFeed();

             });

          }, function (err) {

             $scope.sending = false;
             $scope.showMessage('danger', err.data.message.reason);

          });

        }

    };

    $scope.openNote = function () {

        $('#noteModal').modal('show');

    };

    $scope.closeNote = function () {

        $scope.note = {};
        $scope.note.message = '';
        $('#noteModal').modal('hide');

    };

    $scope.addNote = function(note) {

        $scope.adding = true;

        var message = {};

        message.type = "note";
        message.time = new Date();
        message.who = "Note";
        message.message = note.message;
        message.id = $scope.profile.id;
        message.event = $scope.event.id;

        ActivityService.createFeed(message).then(function (response){

            $scope.adding = false;
            $scope.showMessage('success', response.data.message);
            $scope.closeNote();

            getFeed();

        }, function (err) {

            $scope.adding = false;
            $scope.showMessage('danger', err.data.message.reason);
            $scope.closeNote();

        });

    };

    $scope.openSummaryModal = function () {

      $('#sendSummaryModal').modal('show');

    };

    $scope.addEmail = function (email, form) {

      $scope.emails.push(email.newEmail);

      form.$setPristine();
      $scope.email.newEmail = '';
      $scope.email = {};

    };

    $scope.editEmail = function (email) {

      var index = $scope.emails.indexOf(email);
      $scope.emails[index] = email;

    };

    $scope.removeEmail = function (email) {

      var index = $scope.emails.indexOf(email);
      $scope.emails.splice(index, 1);

    };

    // Use Functions
    getLocations();
    $scope.checkStatus();

    $scope.loading = true;

    $(document).ready(function () {

        initialize();
        $scope.loading = false;

    });

    searchUsers();
    checkEvent();
    getFeed();

}]);
