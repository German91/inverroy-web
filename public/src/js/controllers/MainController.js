'use strict';

angular.module('inverroy').controller('MainController', ['$scope', 'AuthService', '$state', '$rootScope', 'EventService', '$timeout', function ($scope, AuthService, $state, $rootScope, EventService, $timeout) {

    AuthService.checkUser();

    $scope.$watch(AuthService.logged, function (logged) {

        $scope.loading = true;

        $scope.isLogged = logged;
        $scope.profile  = AuthService.currentUser();


        if (!$scope.isLogged) {

            $state.go('login');

        } else {

            $scope.loading = false;
            $state.go('dashboard');

        }

    });

    $scope.logout = function () {

        AuthService.logout();

    };

    $scope.$watch('message', function (newValue, oldValue) {

        if (newValue) {

            $scope.message = {};

        }

    });

    $scope.showMessage = function (type, message) {

      $.notify({

        message: message

      }, {

        type: type,
        animate: {
          enter: 'animated fadeInRight',
          exit: 'animated fadeOutRight'
        },
        offset: {
          x: 50,
          y: 100
        },
        delay: 2000

      });

    };

    // Variables
    $rootScope.active = false;
    $rootScope.event = {};

    $scope.checkStatus = function () {

        $rootScope.loadingActive = true;

        EventService.getActive().then(function (response) {
            $rootScope.loadingActive = false;

            if (!response.data.data || response.data.data === undefined) {
                $scope.active = false;
            } else {

                if (response.data.data.custom_fields.status === "active") {

                  $scope.active = true;
                  $scope.event = response.data.data;

                } else {

                  $scope.active = false;

                }

            }


        }, function (err) {

            $rootScope.loadingActive = false;
            $scope.active = false;

        });

    };

    $scope.checkStatus();


}]);
