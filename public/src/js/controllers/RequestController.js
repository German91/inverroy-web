'use strict';

angular.module('inverroy').controller('RequestController', [
    '$scope',
    'EventService',
    'AuthService',
    '$state',
    'Pagination',
    'RequestService',
    'FileService',

    function ($scope, EventService, AuthService, $state, Pagination, RequestService, FileService) {


    AuthService.checkUser();

    if ($scope.isLogged) {

      $state.go('dashboard.requests');

    } else {

      $state.go('login');

    }

    //Variables
    $scope.requests = [];
    $scope.file = {};
    $scope.loadingRequests = true;
    $scope.pagination = Pagination.getNew(15);

    // Get all requests
    function getAll() {

      RequestService.all().then(function (response) {

        $scope.loadingRequests = false;
        $scope.requests = response.data.data;

        if ($scope.requests) {
            $scope.pagination.numPages = Math.ceil($scope.requests.length/$scope.pagination.perPage);
        }


      }, function (err) {

        $scope.loadingRequests = false;
        $scope.showMessage('danger', err.data.message.reason);

      });

    }

    $scope.openModal = function (id) {

      $scope.file = {};

      FileService.download(id).then(function (response) {

          // window.open(response.data.data.url, '_blank');
          $scope.file = response.data.data;

      }, function (err) {

        $scope.showMessage('danger', err.data.message.reason);

      });

      $('#openFileModal').modal('show');

    };

    $scope.closeModal = function () {

      $scope.file = {};
      $('#openFileModal').modal('hide');

    };

    $scope.openFile = function () {

      window.open($scope.file.url, '_blank');
      $('#openFileModal').modal('hide');

    };

    // Run functions
    getAll();

}]);
