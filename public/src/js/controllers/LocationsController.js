'use strict';

angular.module('inverroy').controller('LocationsController', ['$scope', '$state', 'AuthService', 'LocationService', '$timeout', 'Pagination', function ($scope, $state, AuthService, LocationService, $timeout, Pagination) {

    AuthService.checkUser();

    if ($scope.profile.role == "Admin" || $scope.profile.role == "Super Admin") {

        if ($scope.isLogged) {

            $state.go('dashboard.locations');

        } else {

            $state.go('dashboard');

        }

    } else {

        $state.go('login');

    }

    // Variables
    var geocoder = new google.maps.Geocoder();
    $scope.locations = [];
    $scope.location = {};
    $scope.lat = 55.93334052458581;
    $scope.lon = -3.2139009644180305;
    $scope.map = {center: {latitude: 55.93334052458581, longitude: -3.2139009644180305 }, zoom: 14, control: {} };
    $scope.options = {scrollwheel: false};
    $scope.showMap = false;
    $scope.pagination = Pagination.getNew(15);

    $scope.marker = {
      id: 0,
      coords: {
        latitude: 55.93334052458581,
        longitude: -3.2139009644180305
      },
      options: { draggable: true },
      events: {
        dragend: function (marker, eventName, args) {

          $scope.lat = marker.getPosition().lat();
          $scope.lon = marker.getPosition().lng();

          var latlng = new google.maps.LatLng($scope.lat, $scope.lon);

          geocoder.geocode({ 'latLng': latlng }, function (results, status) {

            $scope.location.address = results[0].formatted_address;

            for (var address in results[0].address_components) {

                if (results[0].address_components[address].types[0] === "country") {

                    $scope.location.country =  results[0].address_components[address].long_name;

                }

            }


          });

          $scope.marker.options = {
            draggable: true,
            labelContent: $scope.location.address,
            labelAnchor: "100 0",
            labelClass: "marker-labels"
          };
        }
      }
    };

    $scope.getAddress = function (address) {

        geocoder.geocode({ 'address': address }, function (results, status) {

            if (status === google.maps.GeocoderStatus.OK) {

                $scope.newcoords = results[0].geometry.location;

                $scope.marker.coords.latitude = $scope.newcoords.lat();
                $scope.marker.coords.longitude = $scope.newcoords.lng();

                $scope.map.center.latitude = $scope.newcoords.lat();
                $scope.map.center.longitude = $scope.newcoords.lng();

                $scope.location.address = results[0].formatted_address;
                $scope.location.latitude = $scope.newcoords.lat();
                $scope.location.longitude = $scope.newcoords.lng();

                $scope.$apply();

                for (var address in results[0].address_components) {

                    if (results[0].address_components[address].types[0] === "country") {

                        $scope.location.country =  results[0].address_components[address].long_name;

                    }

                }
            }

        });

    };

    $scope.getAddress2 = function (address) {

        geocoder.geocode({ 'address': address }, function (results, status) {

            if (status === google.maps.GeocoderStatus.OK) {

                $scope.newcoords = results[0].geometry.location;

                $scope.marker2.coords.latitude = $scope.newcoords.lat();
                $scope.marker2.coords.longitude = $scope.newcoords.lng();

                $scope.map2.center.latitude = $scope.newcoords.lat();
                $scope.map2.center.longitude = $scope.newcoords.lng();

                $scope.currentlocation.address = results[0].formatted_address;
                $scope.currentlocation.latitude = $scope.newcoords.lat();
                $scope.currentlocation.longitude = $scope.newcoords.lng();

                $scope.$apply();

                for (var address in results[0].address_components) {

                    if (results[0].address_components[address].types[0] === "country") {

                        $scope.currentlocation.country =  results[0].address_components[address].long_name;

                    }

                }
            }

        });

    };

    function getLocations() {

        $scope.loadinglocations = true;

        LocationService.getAll().then(function (response) {

            $scope.loadinglocations = false;
            $scope.locations = response.data.data;

            if ($scope.locations) {
              $scope.pagination.numPages = Math.ceil($scope.locations.length/$scope.pagination.perPage);
            }


        }, function (err) {

            $scope.loadinglocations = false;
            $scope.showMessage('danger', err.data.message.reason);

        });

    }

    $scope.createLocation = function (location) {

        $scope.creatinglocation = true;

        LocationService.create($scope.location).then(function (response) {

            $scope.creatinglocation = false;
            $scope.showMessage('success', response.data.message);

            getLocations();

        }, function (err) {

            $scope.creatinglocation = false;
            $scope.showMessage('danger', err.data.message.reason);

        });

    };

    $scope.updateLocation = function (location) {

        $scope.updatingLocation = true;

        LocationService.updateLocation(location.id, location).then(function (response) {

            $scope.updatingLocation = false;
            $scope.showMessage('success', response.data.message);

            getLocations();

            $('#editlocationModal').modal('hide');

        }, function (err) {

            $scope.updatingLocation = false;
            $scope.showMessage('danger', err.data.message.reason);

        });


    };


    $scope.deleteLocation = function (placeId) {

        $scope.removingLocation = true;

        LocationService.deleteLocation(placeId).then(function (response) {

            $scope.removingLocation = false;
            getLocations();

        }, function (err) {

            $scope.removingLocation = false;
            $scope.showMessage('danger', err.data.message.reason);

        });

    };

    $scope.editLocation = function (location) {

        $scope.showMap = false;
        $scope.currentlocation = {};
        $scope.loadingPlace = true;

        LocationService.getPlace(location.id).then(function(response) {

            $scope.loadingPlace = false;
            $scope.currentlocation = response.data.data;

        }, function (err) {

            $scope.loadingPlace = false;
            $scope.showMessage('danger', err.data.message.reason);

        });



        $('#editlocationModal').modal('show');



        $timeout(function () {
            $scope.map2 = {};
            $scope.marker2 = {};
            $scope.showMap = true;
            $scope.map2 = {center: {latitude: $scope.currentlocation.latitude, longitude: $scope.currentlocation.longitude }, zoom: 14, control: {} };
            $scope.marker2 = {
              id: 0,
              coords: {
                latitude: $scope.currentlocation.latitude,
                longitude:  $scope.currentlocation.longitude
              },
              options: { draggable: true },
              events: {
                dragend: function (marker2, eventName, args) {

                  $scope.lat = marker2.getPosition().lat();
                  $scope.lon = marker2.getPosition().lng();

                  var latlng = new google.maps.LatLng($scope.lat, $scope.lon);

                  geocoder.geocode({ 'latLng': latlng }, function (results, status) {

                    $scope.currentlocation.address = results[0].formatted_address;

                    for (var address in results[0].address_components) {

                        if (results[0].address_components[address].types[0] === "country") {

                            $scope.currentlocation.country =  results[0].address_components[address].long_name;

                        }

                    }


                  });

                  $scope.marker2.options = {
                    draggable: true,
                    labelContent: $scope.currentlocation.address,
                    labelAnchor: "100 0",
                    labelClass: "marker-labels"
                  };
                }
              }
            };

        }, 1000);

    };

    getLocations();


}]);
