'use strict';

angular.module('inverroy').controller('ProfileController', ['$scope', 'UserService', 'AuthService', '$state', function ($scope, UserService, AuthService, $state) {

    AuthService.checkUser();


    if ($scope.isLogged) {

        $state.go('dashboard.profile');

    } else {

        $state.go('login');

    }

    // Variables
    $scope.message = {};
    $scope.updatingPassword = false;

    $scope.updatePassword = function (user) {

        $scope.updatingPassword = true;

        UserService.updatePassword(user).then(function (response) {

            $scope.updatingPassword = false;

            $scope.showMessage('success', response.data.message);

        }, function (err) {

            $scope.updatingPassword = false;

            $scope.showMessage('danger', response.data.message);

        });

    };

    $scope.updateProfile = function (profile) {

        $scope.updating = true;

        UserService.update(profile.id, profile).then(function (response) {

            $scope.updating = false;
            $scope.profile = response.data.data;

            $scope.showMessage('success', response.data.message);


        }, function (err) {

            $scope.updating = false;

            $scope.showMessage('danger', response.data.message);

        });

    };


}]);
