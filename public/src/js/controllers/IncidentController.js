'use strict';

angular.module('inverroy').controller('IncidentController', [
  '$scope',
  '$state',
  'AuthService',
  'EventService',
  'Pagination',
  'EmailService',
  'RequestService',
  'FileService',

  function ($scope, $state, AuthService, EventService, Pagination, EmailService, RequestService, FileService) {

    AuthService.checkUser();

    if ($scope.isLogged) {

        $state.go('dashboard.incidents');

    } else {

        $state.go('login');

    }

    // Variables
    $scope.incidents = [];
    $scope.incident = {};
    $scope.file = {};
    $scope.loadingincidents = true;
    $scope.pagination = Pagination.getNew(15);

    function getAll() {

      EventService.getAll().then(function (response) {

          $scope.loadingincidents = false;
          $scope.incidents = response.data.data;

          if ($scope.incidents) {
            $scope.pagination.numPages = Math.ceil($scope.incidents.length/$scope.pagination.perPage);
          }


      }, function (err) {

          $scope.loadingincidents = false;
          $scope.showMessage('danger', err.data.message.reason);

      });

    }

    // Functions
    $scope.removeIncident = function (id) {

      swal({
        title: 'Are you sure?',
        text: "The incident will be archived. You will be able to see it in the View Archive section",
        type: 'warning',
        showCancelButton: true,
        buttonsStyling: false,
        confirmButtonClass: 'btn button-blue margin-right',
        cancelButtonClass: 'btn button-red',
        confirmButtonText: 'Yes, archive it!'
      }).then(function() {
        var eventId = id;

        EventService.remove(eventId).then(function (response) {

          $scope.showMessage('success', response.data.message);
          getAll();


        }, function (err) {

          $scope.showMessage('danger', err.data.message.reason);

        });
      })

    };


    $scope.openModal = function (id) {

      $scope.file = {};

      FileService.download(id).then(function (response) {

          // window.open(response.data.data.url, '_blank');
          $scope.file = response.data.data;

      }, function (err) {

        $scope.showMessage('danger', err.data.message.reason);

      });

      $('#openFileModal').modal('show');

    };

    $scope.closeModal = function () {

      $scope.file = {};
      $('#openFileModal').modal('hide');

    };

    $scope.openFile = function () {

      window.open($scope.file.url, '_blank');
      $('#openFileModal').modal('hide');

    };

    getAll();

}]);
