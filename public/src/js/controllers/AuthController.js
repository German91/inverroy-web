'use strict';

angular.module('inverroy').controller('AuthController', ['$scope', 'AuthService', '$state', '$stateParams', function ($scope, AuthService, $state, $stateParams) {


    $('body').addClass('loginBackground');


    AuthService.checkUser();


    if ($scope.isLogged) {

        $state.go('dashboard');

    }

    $scope.login = function (user, loginForm) {

        AuthService.login(user);

    };

    $scope.recoverPassword = function (user, recoverForm) {

        var email = user.email;

        AuthService.recover(email).then(function (response) {

            $scope.showMessage('success', response.data.message);

            recoverForm.$setPristine();
            $scope.user = {};
            $scope.user.email = '';


        }, function (err) {

            $scope.showMessage('danger', response.data.message);

        });

    };

    $scope.setPassword = function (user) {

      user.reset_password_token = $stateParams.reset_password_token;

      AuthService.setPassword(user).then(function (response) {
        console.log(response);
      }, function (err) {
        console.log(err);
      });

    };

}]);
