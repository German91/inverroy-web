'use strict';

angular.module('inverroy').controller('UsersController', ['$scope', 'UserService', 'AuthService', '$state', 'Pagination',  function ($scope, UserService, AuthService, $state, Pagination) {


    AuthService.checkUser();

    if ($scope.profile.role == "Admin" || $scope.profile.role == "Super Admin") {

        if ($scope.isLogged) {

            $state.go('dashboard.users');

        } else {

            $state.go('dashboard');

        }

    } else {

        $state.go('login');

    }

    //Variables
    $scope.users = [];
    $scope.user = {};
    $scope.newuser = {};
    $scope.loadingUsers = true;
    $scope.pagination = Pagination.getNew(15);
    $scope.sortType = 'user.first_name';
    $scope.sortReverse = false;

    // Functions
    function loadUsers() {

        UserService.getAll().then(function (response) {

            $scope.loadingUsers = false;
            $scope.users = response.data.data;

            if ($scope.users) {
              $scope.pagination.numPages = Math.ceil($scope.users.length/$scope.pagination.perPage);
            }


        }, function (err) {

            $scope.loadingUsers = false;
            $scope.showMessage('danger', err.data.message.reason);

        });

    }

    $scope.changeIcon = function(id) {

        var id = '#span_' + id;

        var icon = $(id);

        if (icon.hasClass('glyphicon-menu-right')) {

            icon.removeClass('glyphicon-menu-right');
            icon.addClass('glyphicon-menu-down');

        } else if (icon.hasClass('glyphicon-menu-down')) {

            icon.removeClass('glyphicon-menu-down');
            icon.addClass('glyphicon-menu-right');

        }

    };


    $scope.deleteUser = function (id) {

      swal({
        title: 'Are you sure?',
        text: "The User will be removed",
        type: 'warning',
        showCancelButton: true,
        buttonsStyling: false,
        confirmButtonClass: 'btn button-blue margin-right',
        cancelButtonClass: 'btn button-red',
        confirmButtonText: 'Yes, remove it!'
      }).then(function() {
        $scope.removingUser = true;

        UserService.delete(id).then(function (response) {

            $scope.removingUser = false;
            loadUsers();

        }, function (err) {

            $scope.removingUser = false;
            $scope.showMessage('danger', err.data.message.reason);

        });
      })

    };


    $scope.createUser = function (user, form) {

        form.$setPristine();
        $scope.newuser = {};
        $scope.newuser.name         = '';
        $scope.newuser.department   = '';
        $scope.newuser.role         = '';
        $scope.newuser.crisis_role  = '';
        $scope.newuser.mobile_phone = '';
        $scope.newuser.work_phone   = '';
        $scope.newuser.work_phone   = '';
        $scope.newuser.home_phone   = '';
        $scope.newuser.email        = '';

        $scope.creatingUser = true;

        UserService.create(user).then(function (response) {

            $scope.creatingUser = false;
            $scope.showMessage('success', response.data.message);

            loadUsers();

        }, function (err) {

            $scope.creatingUser = false;
            $scope.showMessage('danger', err.data.message.reason);

        });

    };

    $scope.editUser = function (user) {

        $scope.user = user;
        $('#edituserModal').modal('show');

    };

    $scope.updateUser = function (user) {

        var userId = user.id;
        $scope.updatingUser = true;

        UserService.update(userId, user).then(function (response) {

            $scope.updatingUser = false;
            $scope.showMessage('success', response.data.message);

            $('#edituserModal').modal('hide');

        }, function (err) {

            $scope.updatingUser = true;
            $scope.showMessage('danger', err.data.message.reason);

        });

    };

    $scope.addUsers = function (file) {

        var objs = [];
        var users = [];
        var toCreate = [];

        for (var i = 0; i < file.length; i++) {
            objs.push(file[i]);
        }

        for (var obj in objs) {

            users.push(objs[obj][0].split(','));

        }

        users.shift();

        for (var user in users) {

            var newUser = {};
            newUser.name = users[user][0];
            newUser.department = users[user][1];
            newUser.role = users[user][2];
            newUser.crisis_role = users[user][3];
            newUser.mobile_phone = users[user][4];
            newUser.work_phone = users[user][5];
            newUser.home_phone = users[user][6];
            newUser.email = users[user][7];

            toCreate.push(newUser);

        }

        for (var newUser in toCreate) {

            $scope.createUser(toCreate[newUser]);

        }

    };


    loadUsers();

}]);
