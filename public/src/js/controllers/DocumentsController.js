'use strict';

angular.module('inverroy').controller('DocumentsController', ['$scope', '$state', 'AuthService', 'Upload', 'FileService', 'Pagination', function ($scope, $state, AuthService, Upload, FileService, Pagination) {

    AuthService.checkUser();

    if ($scope.profile.role == "Admin" || $scope.profile.role == "Super Admin") {

        if ($scope.isLogged) {

            $state.go('dashboard.documents');

        } else {

            $state.go('dashboard');

        }

    } else {

        $state.go('login');

    }

    // Variables
    $scope.allfiles = [];
    $scope.currentDocument = {};
    $scope.pagination = Pagination.getNew(15);

    function getFiles() {

      $scope.allfiles = [];

        $scope.loadingFiles = true;

        FileService.getFiles().then(function (response) {

            $scope.loadingFiles = false;

            for (var file in response.data.data) {

              if ($scope.profile.custom_fields.crisis_role === 'BRTChair' && response.data.data[file].custom_fields.brtchair) {

                $scope.allfiles.push(response.data.data[file])

              }

              if ($scope.profile.custom_fields.crisis_role === 'BRTMember' && response.data.data[file].custom_fields.brtmember) {

                $scope.allfiles.push(response.data.data[file])

              }

              if ($scope.profile.custom_fields.crisis_role === 'other' && response.data.data[file].custom_fields.other) {

                $scope.allfiles.push(response.data.data[file])

              }

              if ($scope.profile.custom_fields.crisis_role === 'CMT' && response.data.data[file].custom_fields.cmt) {

                $scope.allfiles.push(response.data.data[file])

              }

            }

            if ($scope.allfiles) {
              $scope.pagination.numPages = Math.ceil($scope.allfiles.length/$scope.pagination.perPage);
            }


        }, function (err) {

            $scope.loadingFiles = false;
            $scope.showMessage('danger', err.data.message.reason);

        });

    }

    $scope.uploadFile = function (file) {

        $scope.loadingImage = true;

        $scope.upload = Upload.upload({

          url     : '/api/files/',
          method  : 'POST',
          data    : { file: file }

        }).then(function(response) {

           $scope.loadingImage = false;
           $scope.showMessage('success', response.data.message);

           getFiles();

        }, function (err) {

            $scope.loadingImage = false;
            $scope.showMessage('danger', err.data.message.reason);

        });

    };

    $scope.deleteFile = function () {

        $scope.deletingFile = true;

        FileService.deleteFile($scope.currentDocument.id).then(function (response) {

            $scope.deletingFile = false;
            $scope.showMessage('success', response.data.message);

            getFiles();
            $('#removefileModal').modal('hide');

        }, function (err) {

            $scope.deletingFile = false;
            $scope.showMessage('danger', err.data.message.reason);
            $('#removefileModal').modal('hide');

        });

    };

    $scope.documentModal = function (file_id) {

        FileService.searchFile(file_id).then(function (response) {

            $scope.currentDocument = response.data.data;

            $('#documentModal').modal('show');

        });

    };

    $scope.updateFile = function (currentDocument) {

        $scope.updatingFile = true;

        var file_id = currentDocument.id;
        var file = {};

        file.name = currentDocument.name;
        file.brtmember = currentDocument.custom_fields.brtmember;
        file.brtchair = currentDocument.custom_fields.brtchair;
        file.cmt = currentDocument.custom_fields.cmt;
        file.other = currentDocument.custom_fields.other;

        FileService.updateFile(file_id, file).then(function (response) {

            $scope.updatingFile = false;
            $scope.showMessage('success', response.data.message);

            $('#documentModal').modal('hide');

            getFiles();

        }, function (err) {

            $scope.updatingFile = false;
            $scope.showMessage('danger', err.data.message.reason);

        });

    };

    $scope.openModal = function(file_id) {

      FileService.searchFile(file_id).then(function (response) {

            $scope.currentDocument = response.data.data;
            $('#removefileModal').modal('show');

        });

    };

    $scope.closeModal = function () {

      $scope.currentDocument = {};
      $('#removefileModal').modal('hide');

    };


    getFiles();


}]);
