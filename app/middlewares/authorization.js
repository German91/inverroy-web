'use strict';

exports.isSuperAdmin = function (req, res, next) {
    var user = req.session.user;

    if (!user) {

        res.redirect('/');

    } else {

        if (user.role === "Admin" || user.role === "Super Admin") {

            return next();

        } else {

            res.redirect('/');

        }

    }
};


exports.isAdmin = function (req, res, next) {
    var user = req.session.user;

    if (!user) {

        res.redirect('/');

    } else {

        if (user.role === "Incident") {

            return next();

        } else {

            res.redirect('/');

        }

    }
};


exports.isLogged = function (req, res, next) {
    var user = req.session.user;

    if (user) {

        return next();

    } else {

        res.redirect('/');

    }
};
