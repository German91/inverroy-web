'use strict';

if (process.env.NODE_ENV !== 'production') {

    require('dotenv').config();

}

const ArrowDB = require('arrowdb');

const arrowDBApp = new ArrowDB(process.env.APP_KEY, {
    apiEntryPoint: process.env.BASE_URL,
    autoSessionManagement: true,
    prettyJson: true,
    responseJsonDepth: 3
});

module.exports = arrowDBApp;
