'use strict';

const arrow = require('../config/arrowdb');


exports.getCheck = function (req, res, next) {

  var event_id = req.params.event_id;

  arrow.checkinsQuery({

      where: {

          event_id: event_id

      },
      limit: 1000,
      acl_name: "adminAccess"

  }, function (err, result) {

      if (err) {

          res.status(500).json({ type: "error", message: err });
          return next();

      } else {

          var checkins = result.body.response.checkins;

          res.status(200).json({ type: "success", data: checkins });

      }

  });

};


