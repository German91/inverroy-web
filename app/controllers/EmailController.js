'use strict';

const arrow = require('../config/arrowdb');

if (process.env.NODE_ENV !== 'production') {

    require('dotenv').config();

}


exports.sendEmail = function (req, res, next) {

  var recipient   = req.body.recipients;
  var incident    = req.body.incident;
  var fromEmail   = req.body.fromEmail;
  var template    = "Summary of " + incident;

  arrow.emailsSend({

      template: template,
      recipients: recipients,
      from: fromEmail,
      content_type: multipart,
      acl_name: "adminAccess"

  }, function (err, result) {

      if (err) {

          res.status(500).json({ type: "error", message: err });
          return next();

      } else {

          res.status(200).json({ type: "success", message: "Summary successfully created" });

      }

  });

};

exports.sendRequest = function (req, res, next) {

  var user = req.body.email;
  var name = req.body.first_name;

  arrow.emailsSend({

      template: 'Request',
      recipients: 'germanhernandez.dr@gmail.com',
      first_name: name,
      content_type: 'html',
      from: user,
      pretty_json: true,
      acl_name: "adminAccess"

  }, function (err, result) {

      if (err) {

          res.status(500).send({ type: 'error', message: err });
          return next();

      } else {

          res.status(200).send({ type: 'success', message: 'Request successfully sent' });

      }

  });

};


exports.sendSummary = (req, res, next) => {

  let file      = req.body.file;
  let emails    = req.body.who;
  let message   = req.body.message;
  let isReady   = false;

  let from = 'german@gearedapp.co.uk';


  setTimeout(function () {
    arrow.filesShow({

        file_id: file,
        acl_name: "adminAccess"

    }, function (err, result) {

        if (err) {

            res.status(500).json({ type: "error", message: err });
            return next();

        } else {

          let file_url = result.body.response.files[0].url;

          arrow.emailsSend({

              template: 'summary',
              recipients: emails,
              link: file_url,
              message: message,
              content_type: 'html',
              from: from,
              pretty_json: true,
              acl_name: "adminAccess"

          }, function (err, result) {

              if (err) {

                  res.status(500).send({ type: 'error', message: err });
                  return next();

              } else {

                  res.status(200).send({ type: 'success', message: 'Summary successfully created' });

              }

          });

        }

    });

  }, 15000);

};
