'use strict';

const arrow = require('../config/arrowdb');


exports.getOne = function (req, res, next) {

    var eventId = req.params.id;

    arrow.eventsQuery({

        where: {
            id: eventId
        }

    }, function (err, data) {

        if (err) {

            res.status(500).send({ type: 'error', message: err });
            return next();

        } else {

            var incident = data.body.response.events[0];

            res.status(200).send({ type: 'success', data: incident });

        }

    });

};


exports.createEvent = function (req, res, next) {

    if (req.body.hasTemporary) {

        var place_name      = req.body.temporary_name;
        var place_address   = req.body.temporary_address;
        var place_country   = req.body.temporary_country;
        var place_latitude  = req.body.temporary_latitude;
        var place_longitude = req.body.temporary_longitude;
        var name            = req.body.name;
        var phone           = req.body.phone;
        var description     = req.body.description;
        var status          = "active";
        var start_time      = new Date();

        arrow.eventsCreate({

            name: name,
            details: description,
            custom_fields: {
                hasTemporary: true,
                status: status,
                phone: phone,
                temporary_name: place_name,
                temporary_address: place_address,
                temporary_country: place_country,
                temporary_latitude: place_latitude,
                temporary_longitude: place_longitude,
                hidden: false
            },
            start_time: start_time,
            acl_name: 'adminAccess'

        }, function (err, result) {

            if (err) {

                res.status(500).json({ type: "error", message: err });
                return next();

            } else {

                var event = result.body.response.events[0];

                res.status(200).json({ type: "success", message: "Incident successfully created", data: event });

            }

        });

    } else {

        var place_id    = req.body.place_id;
        var name        = req.body.name;
        var phone       = req.body.phone;
        var description = req.body.description;
        var status      = "active";
        var start_time  = new Date();

        arrow.eventsCreate({

            name: name,
            place_id: place_id,
            details: description,
            custom_fields: {
                hasTemporary: false,
                status: status,
                phone: phone,
                hidden: false
            },
            start_time: start_time,
            acl_name: 'adminAccess'

        }, function (err, result) {

            if (err) {

                res.status(500).json({ type: "error", message: err });
                return next();

            } else {

                var event = result.body.response.events[0];

                res.status(200).json({ type: "success", message: "Incident successfully created", data: event });

            }

        });

    }

};

exports.getActive = function (req, res, next) {

    arrow.eventsQuery({

        where: {
            status: "active"
        }

    }, function (err, result) {

        if (err) {

            res.status(500).json({ type: "error", message: err });
            return next();

        } else {

            var event = result.body.response.events[0];

            res.status(200).json({ type: "success", data: event });

        }

    });

};


exports.stopIncident = function (req, res, next) {

    var eventId = req.params.eventId;

    arrow.eventsUpdate({

        event_id: eventId,
        custom_fields: {
            status: "inactive"
        }

    }, function (err, result) {

        if (err) {

            res.status(500).json({ type: "error", message: err });
            return next();

        } else {

            res.status(200).json({ type: "success", message: "Incident successfully stoped" });

        }

    });

};


exports.getAll = function (req, res, next) {

    arrow.eventsQuery({

        where: {
            status: "inactive",
            hidden: false
        },
        limit: 1000

    }, function (err, result) {

        if (err) {

            res.status(500).json({ type: "error", message: err });
            return next();

        } else {

            var events = result.body.response.events;

            res.status(200).json({ type: "success", data: events });

        }

    });

};

exports.editEvent = function (req, res, next) {

    var hasTemporary = req.body.hasTemporary;

    var event_id    = req.body.event_id;
    var name        = req.body.name;
    var phone       = req.body.phone;
    var description = req.body.details;

    if (hasTemporary) {

        var place_name      = req.body.temporary_name;
        var place_address   = req.body.temporary_address;
        var place_country   = req.body.temporary_country;
        var place_latitude  = req.body.temporary_latitude;
        var place_longitude = req.body.temporary_longitude;

        arrow.eventsUpdate({

            event_id: event_id,
            name: name,
            details: description,
            custom_fields: {
                phone: phone,
                hasTemporary: true,
                temporary_name: place_name,
                temporary_address: place_address,
                temporary_country: place_country,
                temporary_latitude: place_latitude,
                temporary_longitude: place_longitude,
            }

        }, function (err, result) {

            if (err) {

                res.status(500).json({ type: "error", message: err });
                return next();

            } else {

                var incident = result.body.response.events[0];

                res.status(200).json({ type: "success", message: "Incident successfully updated", data: incident });

            }

        });


    } else {

        var placeId = req.body.place_id;

        arrow.eventsUpdate({

            event_id: event_id,
            name: name,
            details: description,
            custom_fields: {
                hasTemporary: false,
                phone: phone,
                temporary_name: "",
                temporary_address: "",
                temporary_country: "",
                temporary_latitude: "",
                temporary_longitude: ""
            },
            place_id: placeId

        }, function (err, result) {

            if (err) {

                res.status(500).json({ type: "error", message: err });
                return next();

            } else {

                var incident = result.body.response.events[0];

                res.status(200).json({ type: "success", message: "Incident successfully updated", data: incident });

            }

        });


    }

};

exports.remove = function (req, res, next) {

  var id = req.params.eventId;

  arrow.eventsUpdate({

    event_id: id,
    custom_fields: {
      hidden: true
    }

  }, function (err, data) {

    if (err) {

      res.status(500).send({ type: 'error', message: err });
      return next();

    } else {

      res.status(200).send({ type: 'success', message: 'Incident successfully archived' });

    }

  });

};
