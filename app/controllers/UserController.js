'use strict';

var arrow = require('../config/arrowdb');
var generate = require('../middlewares/generatePassword');


exports.getAll = function (req, res, next) {

    arrow.usersQuery({ limit: 1000, acl_name: "adminAccess" }, function (err, result) {

        if (err) {

            res.status(500).json({ type: "error", message: err });
            return next();

        } else {

            var users = result.body.response.users;

            res.status(200).json({ type: "success", data: users });

        }

    });

};


exports.create = function (req, res, next) {

    var email           = req.body.email;
    var first_name      = req.body.name;
    var password        = generate.generatePassword();
    var department      = req.body.department;
    var role            = req.body.role;
    var crisis_role     = req.body.crisis_role;
    var mobile          = req.body.mobile_phone;
    var work            = req.body.work_phone;
    var home            = req.body.home_phone;

    arrow.usersCreate({

        email: email,
        first_name: first_name,
        last_name: first_name,
        password: password,
        password_confirmation: password,
        custom_fields: {
            department: department,
            crisis_role: crisis_role,
            mobile_phone: mobile,
            work_phone: work,
            home_phone: home
        },
        role: role,
        acl_name: "adminAccess"

    }, function (err, result) {

        if (err) {

            res.status(500).json({ type: "error", message: err });
            return next();

        } else {

            arrow.usersRequestResetPassword({

                email: email

            }, function (err, result) {

                if (err) {

                    res.status(500).json({ type: "error", message: err });
                    return next();

                } else {

                    res.status(200).json({ type: "success", message: "User successfully created" });

                }

            });

        }

    });

};


exports.search = function (req, res, next) {

    var userId = req.params.userId;

    arrow.usersShow({

        user_id: userId,
        acl_name: "adminAccess"

    }, function (err, result) {

        if (err) {

            res.status(500).json({ type: "error", message: err });
            return next();

        } else {

            var user = result.body.response.users[0];

            res.status(200).json({ type: "success", data: user });

        }

    });

};


exports.update = function (req, res, next) {

    var userId          = req.params.userId;
    var email           = req.body.email;
    var first_name      = req.body.first_name;
    var department      = req.body.custom_fields.department;
    var role            = req.body.role;
    var crisis_role     = req.body.custom_fields.crisis_role;
    var mobile          = req.body.custom_fields.mobile_phone;
    var work            = req.body.custom_fields.work_phone;
    var home            = req.body.custom_fields.home_phone;

    arrow.usersUpdate({

        su_id       : userId,
        email: email,
        first_name: first_name,
        custom_fields: {
            department: department,
            crisis_role: crisis_role,
            mobile_phone: mobile,
            work_phone: work,
            home_phone: home
        },
        role: role,
        acl_name: "adminAccess"

    }, function (err, result) {

        if (err) {

            res.status(500).json({ type: "error", message: err });
            return next();

        } else {

            var user = result.body.response.users[0];

            res.status(200).json({ type: "success", message: "User successfully updated", data: user });

        }

    });

};


exports.updatePassword = function (req, res, next) {

    var userId = req.body.id;
    var password = req.body.new;
    var confirm = req.body.confirm;

    arrow.usersUpdate({

        password: password,
        password_confirmation: confirm,
        su_id: userId,
        acl_name: "adminAccess"

    }, function (err, result) {

        if (err) {

            res.status(500).json({ type: "error", message: err });
            return next();

        } else {

            res.status(200).json({ type: "success", message: "Password successfully updated" });

        }

    });

};


exports.delete = function (req, res, next) {

    var userId = req.params.userId;

    arrow.usersDelete({ su_id : userId, acl_name: "adminAccess" },function (err, result) {

        if (err) {

            res.status(400).json({ type: "error", message: "User not logged" });
            return next();

        } else {

            res.status(200).json({ type: "success", message: "User successfully removed" });

        }

    });

};


exports.searchRole = function (req, res, next) {

    var role = req.params.role;

    arrow.usersQuery({

        where: {
            crisis_role: role
        },
        acl_name: "adminAccess"

    }, function (err, result) {

        if (err) {

            res.status(500).json({ type: "error", message: err });
            return next();

        } else {

            var users = result.body.response.users;

            res.status(200).json({ type: "success", data: users });

        }

    });

};
