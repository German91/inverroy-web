'use strict';

const arrow = require('../config/arrowdb');


exports.createFeed = function (req, res, next) {

  var type = req.body.type;
  var time = req.body.time;
  var who  = req.body.who;
  var text = req.body.message;
  var user = req.body.id;
  var event = req.body.event;

  arrow.customObjectsCreate({
      classname: 'ActivityFeed',
      fields: {
          type: type,
          time: time,
          who: who,
          text: text,
          event_id: event
      },
      user_id: user,
      acl_name: "adminAccess"
  }, function (err, result) {

      if (err) {

          res.status(500).json({ type: "error", message: err });
          return next();

      } else {

          res.status(200).json({ type: "success", message: "Note successfully created" });

      }

  });

};

exports.getFeed = function (req, res, next) {

  var eventId = req.params.event_id;

  arrow.customObjectsQuery({
      classname: 'ActivityFeed',
      where: {
          event_id: eventId
      }
  }, function (err, result) {

      if (err) {

          res.status(500).json({ type: "error", message: err });
          return next();

      } else {

          var feeds = result.body.response.ActivityFeed;

          res.status(200).json({ type: "success", data: feeds });

      }

  });

};
