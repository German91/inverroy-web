'use strict';

var arrow = require('../config/arrowdb');


exports.pushNotification = function (req, res, next) {

    var message = req.body.text;
    var ids = req.body.ids;

    arrow.pushNotificationsNotify({

        channel: 'message',
        to_ids: ids,
        payload: message,
        acl_name: "adminAccess"

    }, function (err, result) {

        if (err) {

            res.status(500).json({ type: "error", message: err });
            return next();

        } else {

            res.status(200).json({ type: "success", message: "Message successfully sent" });

        }

    });


};
