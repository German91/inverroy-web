'use strict';

const arrow = require('../config/arrowdb');

exports.upload = function (req, res, next) {

    let file = req.files.file;

    arrow.filesCreate({

        name : file.name,
        file : file.path,
        custom_fields: {
            brtmember: true,
            brtchair: true,
            cmt: true,
            other: true,
            hidden: false
        },
        acl_name: "adminAccess"

    }, function (err, result) {

        if (err) {

            res.status(500).json({ type: "error", message: err });
            return next();

        } else {

            res.status(200).json({ type: "success", message: "Uploaded successful" });

        }

    });

};


exports.getFiles = function (req, res, next) {

    arrow.filesQuery({

        limit: 1000,
        acl_name: "adminAccess",
        where: {
          hidden: false
        }

    }, function (err, result) {

        if (err) {

            res.status(500).json({ type: "error", message: err });
            return next();

        } else {

            var files = result.body.response.files;

            res.status(200).json({ type: "success", message: "Uploaded successful", data: files });

        }

    });

};


exports.deleteFile = function (req, res, next) {

    var file_id = req.params.file_id;

    arrow.filesDelete({

        file_id: file_id

    }, function (err, result) {

        if (err) {

            res.status(500).json({ type: "error", message: err });
            return next();

        } else {

            res.status(200).json({ type: "success", message: "Removed file successful" });

        }

    });

};


exports.updateFile = function (req, res, next) {

    var file_id     = req.params.file_id;
    var name        = req.body.name;
    var brtmember   = req.body.brtmember;
    var brtchair    = req.body.brtchair;
    var cmt         = req.body.cmt;
    var other       = req.body.other;

    arrow.filesUpdate({

        file_id: file_id,
        custom_fields: {
            brtmember: brtmember,
            brtchair: brtchair,
            cmt: cmt,
            other: other
        },
        name: name,
        acl_name: "adminAccess"

    }, function (err, result) {

        if (err) {

            res.status(500).json({ type: "error", message: err });
            return next();

        } else {

            res.status(200).json({ type: "success", message: "Updated file successful" });

        }

    });

};


exports.searchFile = function (req, res, next) {

    var file_id = req.params.file_id;

    arrow.filesShow({

        file_id: file_id,
        acl_name: "adminAccess"

    }, function (err, result) {

        if (err) {

            res.status(500).json({ type: "error", message: err });
            return next();

        } else {

            var file = result.body.response.files[0];

            res.status(200).json({ type: "success", data: file });

        }

    });

};


exports.download = function (req, res, next) {

  var eventId = req.params.eventId;

   arrow.filesQuery({

        where: {
          event_id: eventId
        },
        acl_name: 'adminAccess'

    }, function (err, result) {

        if (err) {

            res.status(500).json({ type: "error", message: err });
            return next();

        } else {

            var file = result.body.response.files[0];

            res.status(200).json({ type: "success", data: file });

        }

    });

};
