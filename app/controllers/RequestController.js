'use strict';

const arrow = require('../config/arrowdb');


module.exports = {

  all: function (req, res, next) {

    arrow.eventsQuery({

        where: {
            status: "inactive",
            hidden: true
        },
        limit: 1000

    }, function (err, result) {

        if (err) {

            res.status(500).json({ type: "error", message: err });
            return next();

        } else {

            var requests = result.body.response.events;

            res.status(200).json({ type: "success", data: requests });

        }

    });
  }

};
