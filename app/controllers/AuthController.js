'use strict';

const arrow = require('../config/arrowdb');

if (process.env.NODE_ENV !== 'production') {

    require('dotenv').config();

}


exports.login = function (req, res, next) {

    var user = {

        email : req.body.email,
        password : req.body.password

    };

    var sess = req.session;

    arrow.usersLogin({

        login: user.email,
        password: user.password

    }, function (err, result) {

        if (err) {

            res.status(401).json({ type: "error", message: "The email or password is invalid" });
            return next();

        } else {

            var user = result.body.response.users[0];

            if (user.admin === "true" || user.role === "Admin" || user.role === "Incident") {

                sess.user = user;

                res.status(200).json({ type: "success", message: "Login successful", data: user });

            } else {

                res.status(403).json({ type: "unauthorized", message: "You're not authorized" });

            }

        }

    });

};

exports.logout = function (req, res, next) {

    arrow.usersLogout(function (err, result) {

        if (err) {

            res.status(400).json({ type: "error", message: err });
            return next();

        } else {

            req.session.destroy();

            res.status(200).json({ type: "success", message: "Logged out" });

        }


    });

};

exports.checkUser = function (req, res, next) {

    if (!req.session.user) {

        res.status(200).json({ type: "error", message: "User not logged" });
        return next();

    } else if (req.session.user){

        var user = req.session.user;

        res.status(200).json({ type: "success", message: "User logged", data: user });

    }

};

exports.recover = function (req, res, next) {

    var email = req.params.email;
    let host  = process.env.HOST;

    arrow.usersRequestResetPassword({

        email: email

    }, function (err, result) {

        if (err) {

            res.status(500).json({ type: "error", message: err });
            return next();

        } else {

            res.status(200).json({ type: "success", message: "Check your email in order to set your password" });

        }

    });

};

exports.setPassword = (req, res, next) => {

  let reset_password_token  = req.body.reset_password_token;
  let password              = req.body.password;
  let password_confirmation = req.body.password_confirmation;

  arrow.get('https://api.cloud.appcelerator.com/v1/users/reset_password.json?key=' + process.env.APP_KEY + '&reset_password_token=' + reset_password_token + '&password=' + password + '&password_confirmation=' + password_confirmation, (err, data) => {

    if (err) {

      res.status(500).send({ type: 'error', message: err });
      return next();

    } else {

      res.status(200).send({ type: 'success', message: 'Password successfully setted' });

    }

  });
};
