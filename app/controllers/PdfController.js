'use strict';

const pdf   = require('pdfkit');
const fs    = require('fs');
const arrow = require('../config/arrowdb');

module.exports = {

  createPdf: (req, res, next) => {

    let file    = req.body.data;
    let eventId = req.body.eventId;

    let date = new Date().toISOString().replace(/T/, ' ').replace(/\..+/, '');
    let name = 'summary_' + date + '.pdf';
    let path = './public/dist/' + name;

    let data = fs.writeFile(path, new Buffer(file, 'base64'));

    file = fs.createReadStream(path);

    arrow.filesCreate({

      name: name,
      file: file,
      acl_name: 'adminAccess',
      custom_fields: {
        hidden: true,
        event_id: eventId
      }

    }, function (err, data) {

      if (err) {

        res.status(500).send({ type: 'error', message: err });
        return next();

      } else {

        let file = data.body.response.files[0];

        res.status(200).send({ type: 'success', data: file });

      }

    });

    deleteFile(path);

  }

};

function deleteFile(path) {

  fs.unlink(path, (err) => {
    if (err) throw err;
    console.log('successfully deleted');
  });

}
