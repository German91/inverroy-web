'use strict';

var arrow = require('../config/arrowdb');


exports.create = function (req, res, next) {

    var name        = req.body.name;
    var address     = req.body.address;
    var country     = req.body.country;
    var latitude    = req.body.latitude;
    var longitude   = req.body.longitude;

    arrow.placesCreate({

        name        : name,
        address     : address,
        country     : country,
        latitude    : latitude,
        longitude   : longitude,
        acl_name: "adminAccess"

    }, function (err, result) {

        if (err) {

            res.status(500).json({ type: "error", message: err });
            return next();

        } else {

            var place = result.body.response.places[0];

            res.status(200).json({ type: "success", message: "Location successfully created", data: place });

        }

    });
};


exports.getPlace = function (req, res, next) {

    var placeId = req.params.placeId;

    arrow.placesQuery({

        limit: 1,
        where: {
            id: placeId
        }

    }, function (err, result) {

        if (err) {

            res.status(500).json({ type: "error", message: err });
            return next();

        } else {

            var place = result.body.response.places[0];

            res.status(200).json({ type: "success", data: place});

        }

    });

};


exports.getAll = function (req, res, next) {

    arrow.placesQuery(function (err, result) {

        if (err) {

            res.status(500).json({ type: "error", message: err });
            return next();

        } else {

            var place = result.body.response.places;

            res.status(200).json({ type: "success", data: place});

        }

    });

};


exports.deleteLocation = function (req, res, next) {

    var placeId = req.params.placeId;

    arrow.placesDelete({

        place_id: placeId,

    }, function (err, result) {

        if (err) {

            res.status(500).json({ type: "error", message: err });
            return next();

        } else {

            res.status(200).json({ type: "success", message: "Location successfully removed" });

        }

    });

};


exports.updateLocation = function (req, res, next) {

    var placeId     = req.params.placeId;
    var name        = req.body.name;
    var address     = req.body.address;
    var country     = req.body.country;
    var latitude    = req.body.latitude;
    var longitude   = req.body.longitude;

    arrow.placesUpdate({

        place_id: placeId,
        name        : name,
        address     : address,
        country     : country,
        latitude    : latitude,
        longitude   : longitude

    }, function (err, result) {

        if (err) {

            res.status(500).json({ type: "error", message: err });
            return next();

        } else {

            res.status(200).json({ type: "success", message: "Location successfully updated" });

        }

    });

};
