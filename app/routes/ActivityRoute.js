'use strict';

const express = require('express');
const controller = require('../controllers/ActivityController');
const authorization = require('../middlewares/authorization');

const router = express.Router();

router.route('/').post(authorization.isLogged, controller.createFeed);
router.route('/:event_id').get(authorization.isLogged, controller.getFeed);

module.exports = router;
