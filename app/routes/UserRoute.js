'use strict';

const express = require('express');
const controller = require('../controllers/UserController');
const authorization = require('../middlewares/authorization');

const router = express.Router();

router.route('/')
    .get(authorization.isSuperAdmin, controller.getAll)
    .put(authorization.isLogged, controller.updatePassword)
    .post(authorization.isSuperAdmin, controller.create);

router.route('/:userId')
    .get(authorization.isLogged, controller.search)
    .put(authorization.isLogged, controller.update)
    .delete(authorization.isSuperAdmin, controller.delete);

router.route('/getRole/:role').get(controller.searchRole);

module.exports = router;
