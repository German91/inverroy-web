'use strict';

const express = require('express');
const controller = require('../controllers/NotificationController');
const authorization = require('../middlewares/authorization');

const router = express.Router();

router.route('/').post(authorization.isLogged, controller.pushNotification);


module.exports = router;
