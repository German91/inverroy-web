'use strict';

const express           = require('express');
const RequestController = require('../controllers/RequestController');
const authorization     = require('../middlewares/authorization');

const Router = express.Router();

Router.route('/').get(RequestController.all);


module.exports = Router;
