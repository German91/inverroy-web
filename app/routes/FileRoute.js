'use strict';

const express     = require('express');
const controller  = require('../controllers/FileController');
const multiparty = require('connect-multiparty');
const multipartyMiddleware = multiparty();
const authorization = require('../middlewares/authorization');

const router = express.Router();

router.route('/')
    .post(authorization.isLogged, multipartyMiddleware, controller.upload)
    .get(authorization.isLogged, controller.getFiles);

router.route('/:file_id')
    .get(authorization.isLogged, controller.searchFile)
    .put(authorization.isSuperAdmin, controller.updateFile)
    .delete(authorization.isSuperAdmin, controller.deleteFile);

router.route('/pdf/:eventId').get(authorization.isLogged, controller.download);

module.exports = router;
