'use strict';

const express = require('express');
const controller = require('../controllers/LocationController');
const authorization = require('../middlewares/authorization');

const router = express.Router();

router.route('/')
    .post(authorization.isLogged, controller.create)
    .get(authorization.isLogged, controller.getAll);

router.route('/:placeId')
    .delete(authorization.isSuperAdmin, controller.deleteLocation)
    .put(authorization.isSuperAdmin, controller.updateLocation)
    .get(authorization.isLogged, controller.getPlace);


module.exports = router;
