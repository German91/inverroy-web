'use strict';

const express = require('express');
const controller = require('../controllers/CheckController');
const authorization = require('../middlewares/authorization');

const router = express.Router();


router.route('/:event_id').get(authorization.isLogged, controller.getCheck);


module.exports = router;
