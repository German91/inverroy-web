'use strict';

const express = require('express');
const controller = require('../controllers/EmailController');

const router = express.Router();

router.route('/').post(controller.sendEmail);
router.route('/request').post(controller.sendRequest);
router.route('/summary').post(controller.sendSummary);


module.exports = router;
