'use strict';

const express = require('express');
const controller = require('../controllers/PdfController');
const authorization = require('../middlewares/authorization');

const router = express.Router();

router.route('/').post(authorization.isLogged, controller.createPdf);


module.exports = router;
