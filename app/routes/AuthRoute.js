'use strict';

const express = require('express');
const controller = require('../controllers/AuthController');
const authorization = require('../middlewares/authorization');

const router = express.Router();

router.route('/login')
    .get(controller.checkUser)
    .post(controller.login);

router.route('/logout').get(authorization.isLogged, controller.logout);
router.route('/recover/:email').get(controller.recover);
router.route('/setPassword').get(controller.setPassword);

module.exports = router;
