'use strict';

const express = require('express');
const controller = require('../controllers/EventController');
const authorization = require('../middlewares/authorization');

const router = express.Router();

router.route('/')
    .post(authorization.isLogged, controller.createEvent)
    .get(authorization.isLogged, controller.getAll)
    .put(authorization.isLogged, controller.editEvent);

router.route('/:eventId')
    .put(authorization.isLogged, controller.stopIncident)
    .get(authorization.isLogged, controller.getOne)
    .delete(authorization.isSuperAdmin, controller.remove);

router.route('/getActive').get(authorization.isLogged, controller.getActive);


module.exports = router;
